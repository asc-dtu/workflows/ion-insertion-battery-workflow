" Small and quick unit tests using pytest"
import numpy as np
import pytest
import json
from shutil import copy2

from ase.build import fcc111, bulk, molecule

from iiwPBEU.system_utils import cd, get_wd


class TestFunction:
    """ Test single functions and classes """

    def test_create_unqiue_defect_rm_single(self):
        from iiwPBEU.battery_tools import create_unique_defect
        from ase.io import read
        atoms = read('./test_structures/supercell_mp-676282_4.traj')
        atoms = create_unique_defect(atoms,
                                     ion_ind=0,
                                     ion_out=1,
                                     ion_max=16)
        ion_list = [atom.symbol for atom in atoms if atom.symbol == 'Mg']
        assert len(atoms) == 127
        assert len(ion_list) == 15  # did it really take out Mg


class TestUserInput:
    
    def test_default_input(self):
        from iiwPBEU.input import UserInput
        from jsonschema.exceptions import ValidationError
        with open(UserInput.f_name) as f:
            user_args = json.load(f)
        user_input = UserInput(user_args).validate_user_keys()
        
        user_args_temp = user_args
        user_args["remove_n_ions"] = "all_but_one"
        user_input = UserInput(user_args).validate_user_keys()
        
        with pytest.raises(ValidationError):
            user_args["remove_n_ions"] = "false"
            user_input = UserInput(user_args).validate_user_keys()
        

    def test_material_id(self):
        from iiwPBEU.input import UserInput
        material_ids = ["Pb_/2'", "\\A"]
        for material_id in material_ids:
            with pytest.raises(NameError):
                UserInput.validate_material_id(material_id)


    def test_full_user_input(self, tmp_path):
        """ Including the database setup """
        from iiwPBEU.input import UserInput, FolderStructure
        from ase.db import connect
        with open(UserInput.f_name) as f:
            user_args = json.load(f)
        with cd(str(tmp_path.absolute())):
            wd = get_wd([FolderStructure.starting_structures])
            for db in FolderStructure.expected_starting_dbs:
                test = connect('/'.join((wd, db)))
                test.write(molecule('H2'), 
                           material_id = user_args["material_id"],
                           elements='H')
            UserInput(user_args).validate()
    
    def test_false_user_input(self):
        from iiwPBEU.input import UserInput
        with open(UserInput.f_name) as f:
           user_args = json.load(f)
        user_args["false_key"] = 3
        with pytest.raises(KeyError):
            UserInput(user_args).validate_user_keys()
        
class TestTasksPreparePotneb:
    def test_choose_vacancy_limits(self, tmp_path):
        from iiwPBEU.prepare_potneb_vasp import TaskPreparePotneb
        import os
        n_ion_supercell = 11
        task = TaskPreparePotneb()
        src_stability_out = os.path.abspath('./wf_files/iiwPBEU.check_stability.2498606.out')
        user_args = {'thresholds': { 'ch_energy': 0.5}}
        
        with cd(str(tmp_path.absolute())):
            copy2(src_stability_out, '.')
            
            remove_n_ions = task.choose_vacancy_limits(
                user_args, n_ion_supercell)
            assert remove_n_ions == [1, 10]
            
            user_args['remove_n_ions'] = "single_one"
            remove_n_ions = task.choose_vacancy_limits(
                user_args, n_ion_supercell)
            assert remove_n_ions == [1]
            
            user_args['remove_n_ions'] = "all_but_one"
            remove_n_ions = task.choose_vacancy_limits(
               user_args, n_ion_supercell)
            assert remove_n_ions == [10]
            
            user_args['thresholds']['ch_energy'] = 0
            user_args['remove_n_ions'] = "both_charge_states"
            remove_n_ions = task.choose_vacancy_limits(
                    user_args, n_ion_supercell)
            assert remove_n_ions == [10]
            
            user_args['thresholds']['ch_energy'] = -1
            with pytest.raises(ValueError):
                remove_n_ions = task.choose_vacancy_limits(
                        user_args, n_ion_supercell)
            
            with pytest.raises(NameError):
                user_args['remove_n_ions'] = "false_input"
                remove_n_ions = task.choose_vacancy_limits(
                    user_args, n_ion_supercell)
            

    
class TestDescriptors:
    """ Test all descriptors defined """

    def test_gravimetric_capacity(self):
        from iiwPBEU.descriptors.basic import get_gravimetric_capacity
        atoms = fcc111('Cu', [7, 1, 1], a=3)
        atoms.set_chemical_symbols(['Li', 'Fe', 'P', 'O', 'O', 'O', 'O'])
        Q_th_g = get_gravimetric_capacity(atoms, ion='Li')

        assert np.isclose(Q_th_g, 169.893)

    def test_volumetric_capacity(self):
        from iiwPBEU.descriptors.basic import get_volumetric_capacity
        atoms = fcc111('Cu', [7, 1, 1], a=4, vacuum=3)
        atoms.set_chemical_symbols(['Li', 'Fe', 'P', 'O', 'O', 'O', 'O'])
        Q_th_v = get_volumetric_capacity(atoms, ion='Li')

        assert np.isclose(Q_th_v, 152.946)


class TestPreScreen:
    def test_max_ox_metal(self):
        from iiwPBEU.pre_screen import count_metal_oxidation_state
        formula = 'Mg2Mn4O8'
        ox, max_ox, metal = count_metal_oxidation_state(formula)
        assert [ox, max_ox, metal] == [3, 6, 'Mn']
        ox, max_ox, frac_rm = count_metal_oxidation_state(formula, ion='Mg')
        assert frac_rm == 1
        
class TestImport:
    """ Mostly to see if relax scripts work """

    def test_import_relax(self):
        from iiwPBEU.relax_neb_vasp import relax_neb
        from iiwPBEU.relax_cirneb_vasp import relax_cirneb


