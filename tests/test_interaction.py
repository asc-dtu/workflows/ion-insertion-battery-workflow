"""
Tests aiming at checking if workflow logic and interaction works

@author: felix
"""
import numpy as np
import os
from shutil import copy2

from iiwPBEU.system_utils import cd


class TestWorkflowBlocks:
    """ Workflow blocks are either: prepare, relax or check """
    
    def test_prepare_potneb(self, tmp_path):
        """ Also tests the InequivalentSiteFinder class """
        from iiwPBEU.prepare_potneb_vasp import TaskPreparePotneb
        from ase.io import read
        import os
        import json
        src_bulk = os.path.abspath('./bulk')
        src_bulk_empty = os.path.abspath('./bulk_empty')
        current_mat_src = os.path.abspath('current_material.json')
        src_stability_out = os.path.abspath('./wf_files/iiwPBEU.check_stability.2498606.out')

        with cd(str(tmp_path.absolute())):
            # create symlinks
            os.symlink(src_bulk, './bulk')
            os.symlink(src_bulk_empty, './bulk_empty')
            copy2(current_mat_src, '.')
            copy2(src_stability_out, '.')
            task = TaskPreparePotneb()
            task.run()
            with open("potneb_dictionary.json") as f:
                dic = json.load(f)
            eq_to = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            all_ind = [0, 1, 16, 17, 32, 33, 48, 49, 64, 65, 80, 81, 96, 97, 112, 113]
            unique_ion_pairs = [[0, 97], [0, 33]]
            # also tests site of cell since if it fails, cant be same supercell size
            assert dic['equal_to'] == eq_to
            assert dic['all_ion_indices'] == all_ind
            assert dic['unique_ion_pairs'] == unique_ion_pairs
            assert len(dic['pot_folders']) == 2
            assert len(dic['neb_folders']) == 4
            assert len(dic['neb_folders'][0]) == 2
            assert dic['neb_folders'][0][0].split("/")[-4] == "0_97_1_16"
            # assert supercell is also there
            supercell_f = [f for f in os.listdir() if f.startswith('supercell')][0]
            atoms = read(supercell_f)
            
    def test_check_potential(self, tmp_path):
        """ Test the check potential block with mock values """
        from iiwPBEU.system_utils import cd, get_wd
        from ase.db import connect
        from ase.build import molecule
        from iiwPBEU.check_potential import check_potential
        from ase.calculators.singlepoint import SinglePointCalculator
        
        src_bulk = os.path.abspath('./bulk')
        current_mat_src = os.path.abspath('current_material.json')
        
        with cd(str(tmp_path.absolute())):
            # create mock reference database
            db_wd = get_wd(['starting_structures'])
            with cd(db_wd):
                with connect('references_ch.db') as db:
                    atoms = molecule('H2')
                    atoms.set_chemical_symbols(['Mg', 'Mg'])
                    atoms.set_calculator(
                        SinglePointCalculator(atoms, **{'energy': -4}))
                    db.write(atoms,
                        elements='Mg', energy_per_atom=-2)
            # create symlinks
            mat_home_folder = get_wd([
                'calculations', 'Mg', 'Mg2Mo6S8_mp-676282'])
            with cd(mat_home_folder):
                os.symlink(src_bulk, './bulk')
                pot_wd = get_wd(['potential', '1_2_0', 'FM', 'vasp_rx_PBESOL_U'])
                copy2('./bulk/FM/vasp_rx_PBESOL_U/OUTCAR', pot_wd)
                copy2(current_mat_src, '.')
                check_potential(atoms_f='OUTCAR')
                
    def test_check_stability(self, tmp_path):
        from iiwPBEU.system_utils import cd, get_wd
        from ase.db import connect
        from ase.build import fcc111
        from ase.calculators.singlepoint import SinglePointCalculator
        from iiwPBEU.check_stability import check_stability
        
        src_bulk = os.path.abspath('./bulk')
        src_bulk_empty = os.path.abspath('./bulk_empty')
        current_mat_src = os.path.abspath('current_material.json')
        
        with cd(str(tmp_path.absolute())):
            # create mock reference database
            db_wd = get_wd(['starting_structures'])
            with cd(db_wd):
                with connect('references_ch.db') as db:
                    for element, en in [('Mg', -2), ('Mo', -3), ('S', -2.5)]:
                        atoms = fcc111(element, [1, 1, 1], a=4)
                        newcalc = SinglePointCalculator(atoms, energy=en)
                        atoms.calc = newcalc
                        db.write(atoms, elements=element, energy_per_atom=en)

            # create symlinks
            mat_home_folder = get_wd([
                'calculations', 'Mg', 'Mg2Mo6S8_mp-676282_stability'])
            with cd(mat_home_folder):
                copy2(current_mat_src, '.')
                os.symlink(src_bulk, './bulk')
                os.symlink(src_bulk_empty, './bulk_empty')
                e_ch_full, e_ch_empty, v_change = check_stability()

                assert np.isclose(-4.81, v_change, atol=0.1)
                assert np.isclose(-5.10, e_ch_full, atol=0.1)
                assert np.isclose(-5.598, e_ch_empty, atol=0.1)

