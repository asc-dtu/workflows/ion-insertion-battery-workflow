#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 11:48:25 2020

Script for storing FixVasp unittests

@author: felix
"""
from ase.calculators.emt import EMT
from ase.build import molecule

from iiwPBEU.system_utils import cd

class TestFixVaspApplication:
    """ Test to check fixvasp application does what its supposed to do """
    
    def test_time_out(self, tmp_path):
        """ Test if timeout/memory string finding works """
        from iiwPBEU.fix_dft_calculation import FixVaspCalculation, ErrorStrings
        
        ERROR_STRING = ErrorStrings.time_out

        d = tmp_path / "time_out"
        d.mkdir()
        # create a dummy error file and put string into it
        p = d / "test.err"
        p.write_text(ERROR_STRING)
        
        # dummy arguments
        with cd(str(d.absolute())):
            fixvasp = FixVaspCalculation(atoms=molecule('H2'), calculator=EMT())
            timed_out = fixvasp.calculation_timed_out()
            
        assert timed_out == True
        
    def test_memory_out(self, tmp_path):
        """ Test if timeout/memory string finding works """
        from iiwPBEU.fix_dft_calculation import FixVaspCalculation, ErrorStrings
        
        ERROR_STRING = ErrorStrings.out_of_memory

        d = tmp_path / "time_out"
        d.mkdir()
        # create a dummy error file and put string into it
        p = d / "test.err"
        p.write_text(ERROR_STRING)
        
        # dummy arguments
        with cd(str(d.absolute())):
            fixvasp = FixVaspCalculation(atoms=molecule('H2'), calculator=EMT())
            memory_out = fixvasp.out_of_memory()
            
        assert memory_out == True
        
    def test_new_calc_directory(self, tmp_path):
        from iiwPBEU.fix_dft_calculation import FixVaspCalculation
        
        d = tmp_path / "new_calc"
        d.mkdir()
        # dummy arguments
        with cd(str(d.absolute())):
            fixvasp = FixVaspCalculation(atoms=molecule('H2'), calculator=EMT())
            new_calc = fixvasp.check_new_calc_directory()
            
        assert new_calc == True
