import setuptools



install_requires = [
    'ase>=3.19.1,!=3.21.*', # 3.21.* -> see ASE issue #876
    'spglib==1.12.2.post0',
    'myqueue>=20.1.2,!=21.4.0', # 21.4.0 -> myqueue issue #23
    'jsonschema>=2.6.0',
],


extras_require = {
    'docs': [
        'sphinx',
        'sphinx_rtd_theme',
        'sphinxcontrib-mermaid',
    ],
    'test': [
        'pytest',
        'pytest-cov',
    ]
}


setuptools.setup(
    name="iiwPBEU",
    version="0.1",
    author="Felix Tim Boelle",
    author_email="feltbo@dtu.dk",
    description="Ion insertion workflow using +U correction",
    maintainer='Felix Tim Boelle',
    maintainer_email='feltbo@dtu.dk',
    url="https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=install_requires,
    extras_require=extras_require,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
    ],
)
