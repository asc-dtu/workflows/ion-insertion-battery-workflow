Workflow structure
==================

Details, details, details...

The whole workflow is separated into two separate sub-workflows. In the following, those two workflows as well as their folder structures will be explained.

Subworkflow 1
#############
* Relax bulk structures and create starting structures for NEB calculations

The first workflow ``workflow_start.py`` consists of three seperate tasks where the dependency tree looks like

.. mermaid::

	graph TD
		workflow_start.py --> relax_bulk;
		workflow_start.py --> relax_empty_bulk;
		relax_empty_bulk --> relax_empty_bulk_scaled;
		relax_bulk --> prepare_potneb_structures;

The file ``iiwPBEU.prepare_potneb_vasp.py`` takes care of finding symmetry inequivalent structures and stores this information in the form of folders and a json dictionary. The created folder structure is as follows:


.. mermaid::

	graph TD
	    home_calc_folder --> calculations;
	    home_calc_folder --> starting_structures;
	    calculations -->  ion;
	    ion --> material[material_id];
	    
	    material --> bulk;
	    material --> potential;
	    material --> bulk_empty;
	    material --> bulk_empty_scaled;
	    material --> NEBs;
	    
	    bulk --> mag_state;
	    mag_state --> vasp_rx_functional;

	    bulk_empty --> mag_state1[mag_state];
	    mag_state1 --> vasp_rx1[vasp_rx_functional];
	    
	    bulk_empty_scaled --> mag_state5[mag_state];
	    mag_state5 --> vasp_rx5[vasp_rx_functional];
	    
	    potential --> n_N_uniqueMgIndice;
	    n_N_uniqueMgIndice --> mag_state2[mag_state];
	    mag_state2 --> vasp_rx2[vasp_rx_functional];
	    
	    NEBs --> startInd_finalInd_n_N;
	    startInd_finalInd_n_N --> CIRNEB[CIRNEB/CICNEB];
	    startInd_finalInd_n_N --> RMINEB[RMINEB/CNEB];
	    RMINEB --> mag_state3[mag_state];
	    CIRNEB --> mag_state4[mag_state];
	    mag_state3 --> vasp_rx3[vasp_rx_functional];
	    mag_state4 --> vasp_rx4[vasp_rx_functional];


+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter/Folder description         | Description                                                                                                                                          |
+======================================+======================================================================================================================================================+
| **ion**                              | Ion that will be intercalated or substituted in the structures (e.g. ion = Mg)                                                                       |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **starting\_structures**             | Stores all the unrelaxed starting structures coming from the MaterialsProject database                                                               |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **material_id**                      |Unique material ID provided by the user. This can be an ICSD for example.                                                                             |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **n**                                | is the amount of ions taken out of the supercell structure                                                                                           |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **N**                                | is the maximum amount of ions in the supercell structure                                                                                             |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **uniqueMgIndice**                   | is the atom index in supercell in the ASE atoms object that has to be taken out of the supercell to create a unique structure                        |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **startInd**                         | atom index in supercell of the ion in the structure labelling the NEB starting position                                                              |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **finalInd**                         | atom index in supercell of the ion element in the structure labelling the NEB final position                                                         |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **mag\_state**                       | is the magnetic state                                                                                                                                |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **vasp\_rx\_functional**             | is the folder in which VASP carries out the calculations. The ending *functional (e.g.*\ PBE or *PBE*\ U) indicates which functional was chosen.     |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+
| **CIRNEB/RMINEB/CNEB/CICNEB**        | Specific type of NEB as explained in the paper (CNEB=conventional NEB, CINEB = climbing image CNEB)                                                  |
+--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+

The structures required for the calculations are stored as follows

* **Unrelaxed unit cell:** The unit cell is stored under ``home_calc_folder/starting_structures/materials.db`` .

* **Supercell:** The supercell is stored under ``home_calc_folder/calculations/ion/material_id/`` . The structure is created when calling the script ``prepare_potneb_vasp``. The name of the supercell is stored as f"supercell_{material_id}_{cut_off_neb}.traj".

Subworkflow 2
#############

* Relax potential and NEB structures

The structure of the second workflow ``workflow_potneb.py`` and the dependencies are as follows

.. mermaid::

	graph TD
		workflow_potneb.py --> relax_potential_vasp;
	    relax_potential_vasp --> check_potential{check_potential};
	    check_potential --> |worse|done1(done) ;
	    check_potential --> |better|relax_neb_vasp;
	    relax_neb_vasp --> done2(done);


The NEB part of the workflow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The NEB part of the workflow turned out to be quite involved, since it requires dealing with many different input cases and symmetry considerations. The graph below outlines how the workflow handles these situations.
When keeping the cell shape fixed as done here, it is

.. mermaid::

	graph TD
        case1(initial and final site symmetry equivalent?) --> |yes|reflec(reflection symmetry?);
	    reflec -->|yes|rneb(relax RMI-NEB);
	    rneb-->threshold(Barrier above diffusivity threhsold?)
	    threshold-->|no|rneb2(relax CIR-NEB to check local maximum of middle image)
	    threshold-->|yes|done
	    reflec-->|no|cneb(relax conventional NEB);
	    cneb --> cicneb(CI conventional NEB)
	    case1-->|no|cneb;
	    cicneb-->done
	    rneb2-->done

Collecting the data
###################
There is no automatic storing in a database during runtime. One of the advantages is, that you do not need a database server up and running that can handle concurrent database accesses.
Instead, the data can be collected with the script ``ion-insertion-battery-workflow/workflow/collect.py``.

DFT settings
############

All the details of the DFT settings are stored in the ``iiwPBEU.vasp_setups.py`` file. It also stores the function for consistently relaxing the atoms as well as handling common errors that can occur during a relaxation.

Task definitions
################
All the calculation tasks are structured in the same way. The NEB tasks have slightly different convention but follow the same logic.

.. literalinclude:: ./wf_task.py

The Idea is to structure the relaxation into four steps



1. retrieve information from folder structure: All the information needed to setup the input structure for the calculation


2. get atoms object based on retrieved information: This could be an ICSD structure or a user relaxed structure. In the case of a potential calculation, the indice of the magnesium that needs to be taken out of the supercell atom object is retrieved from the folder structure in step 1.

3. set calculator: Depending on which code is used and what relaxation is carried out specific tags need to be set. All this information is consistently stored in the vasp_setups.py script.

4. relax atoms: A consistent relaxation routine ensure reproducibility. This includes also possible restarts for failed calculations. Moreover, it defines all possible exceptions and errors raised by the user. These can include geoemtrical convergence criterion etc. . A separate error function is also included in the vasp_relaxation_setups.py script that can be extended accordingly.


Perks of having these workflow tasks:

* easier to implement new tasks
* easier to test

Additionally, any task can be executed separately by either inheriting the class and adjusting the input, or by simply running it directly in the appropratie folder using for instance ``mq submit iiwPBEU.relax_bulk_vasp`` .


