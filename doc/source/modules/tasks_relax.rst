Relax Tasks
===============

.. autoclass:: iiwPBEU.relax_bulk_vasp.TaskRelaxBulk
    :members:

.. autoclass:: iiwPBEU.relax_empty_vasp.TaskRelaxBulkEmpty
    :members:

.. autoclass:: iiwPBEU.relax_empty_scaled_vasp.TaskRelaxBulkEmptyScaled
    :members:

.. autoclass:: iiwPBEU.relax_potential_vasp.TaskRelaxBulkPotential
    :members:

