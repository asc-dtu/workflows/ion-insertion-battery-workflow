Installation
============
The workflow was tested and developed for

* ASE 3.19.0
* VASP 5.4.4
* MyQueue 20.1.2
* spglib 1.12.2

Newer versions of ASE and MyQueue are covered by the tests, but other versions of VASP are not guaranteed to work.

Note that this guide is sometimes very specific to the setup of the compute cluster `niflheim <https://wiki.fysik.dtu.dk/niflheim/Niflheim7_Getting_started>`_ at the Technichal University of Denmark. 

Prerequisites
#############

If you are familiar with running VASP using ASE and MyQueue you can skip this part and go straight to :ref:`installing the workflow<Installing the workflow>`.

Running VASP through ASE
^^^^^^^^^^^^^^^^^^^^^^^^
The workflow itself makes use of running VASP calculations through ASE. An
explanation of how to set this up can be found `here <https://wiki.fysik.dtu.dk/ase/ase/calculators/vasp.html>`_ .

If you are lucky, your colleagues set up everything for you. Upon the time of
writing, the fronted MyQueue does not allow to alter the bash submit script upon submission. Therefore, everything has to go into your ``~/.bashrc`` file. Since we want to run the jobs across different cores, you can add the following line to your ``~/.bashrc``:

::

    # node specific module load
    if [[ ! -z "$SLURM_JOB_PARTITION" ]]
    then
        module load VASP
    fi

If this causes problems when running calculations which need different modules, consider adding this line to the ``activate`` script of the virtual environment that we will set up when installing the workflow. For instance, this can happen if you want to run VASP and GPAW calculations at the same time for separate projects. Also have a look at the nice explanation on the `GPAW website <https://wiki.fysik.dtu.dk/gpaw/platforms/Linux/Niflheim/build.html>`_ on this issue.

Running ASE+VASP through MyQueue
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Since keeping track of multiple bash jobs becomes inherently difficult, we use MyQueue. It is a workflow scheduling system and currently supports SLURM/PBS/LSF.

After going through the `documentation <https://myqueue.readthedocs.io>`_ and ensuring you can run VASP calculations through myqueue, you can start with the installation process.

.. note::
    A common error encountered when running VASP using ASE and Myqueue happens, when not specifying to start a single process.
    The submission of any VASP-ASE python script has to follow something like
    
    ``mq submit my_vasp_ase_calc.py -R 8:xeon8:1:1h``

    Note the specification of starting a single process, but requesting 8 cores.


Installing the workflow
#######################
For installing the workflow we will make use of virtual environments in order to avoid interfering with other user installations.

Set up the virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. warning ::
    Before starting make sure to create the virtualenvironment on the "lowest ranking node". At the time of writing this is xeon8. 
    The reason is that creating the virtualenvironment on a specific node is always forward compatible but not backward. That means if creating the virtenv on xeon24, only xeon40 nodes will be able to load the virtenv created with the python on xeon24. On xeon8 and xeon16 you will run into an error message.

First we need to make sure we are using a proper python version. We will not install python, but use a version that is already available on the cluster. We will load python in the ``~/.bashrc`` file by adding these lines:

::

    # modules needed
    module use /home/energy/modules/modules/all # contains VASP installation
    module load Python/3.6.6-intel-2018b

Test that when opening python it will actually open the python version just specified in the ``~/.bashrc``. Within your git repository folder we will now clone the workflow git repository using

::

    $ git clone https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow.git
    $ cd ion-insertion-battery-workflow

If you run into issues, make sure that you have set up GitLab and SSH keys for your account on the cluster. Now we will install the workflow using a virtenv. Start by choosing a name for the virtualenvironment, e.g.

::

    $ python3 -m venv iiw_ase3.19

and add a line to your ``~/.bashrc`` for easy activation, e.g. something like

::

    alias iiw_ase3.19="source /home/niflheim/$USER/iiw_ase3.19/bin/activate"

We will now install all the needed requirements. For this we will activate the virtenv by typing into the terminal

::

    $ iiw_ase3.19

Within this virtual environment we will install the workflow package and its dependencies. Run within the cloned git repository (i.e. the folder containig the ``setup.py`` file)

::

    (iiw_ase3.19) $ pip install --editable .[test]

We choose to install the package in development mode using the *--editbale* flag (short *-e*),meaning that changes to the source directory will immediately affect the installed package without needing to re-install.

You can check that everything went well by running the tests

::
    
    $ cd tests
    $ pytest

Before running the workflow
^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are a few more things that are nice to have.

Consider enabling tab completion for the newly installed myqueue version

::

    (iiw_ase3.19) $ mq completion >> ~/.bashrc

Configuring the queue is done through the ``config.py`` file that should be added to your ``$HOME/.myqueue/`` folder. The best way is to copy this file from someone.

Consider settung up to load the correct cpu architecture for being able to submit to multiple nodes from one single login node. This can be done through creating a file called ``$HOME"/codes/set_cpu_arch`` and in the ``set_cpu_arch`` add these lines

.. literalinclude:: set_cpu_arch

Then source this file by adding it to your ``~/.bashrc``

::

    # set correct submit node
    source "$HOME"/codes/set_cpu_arch

Also make sure to load a clean version of VASP when on the node in your ``~/.bashrc`` to get the correct executable (see also :ref:`Running VASP through ASE`).
