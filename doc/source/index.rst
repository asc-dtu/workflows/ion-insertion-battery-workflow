.. iiwPBEU documentation master file, created by sphinx-quickstart on
.. Wed Mar 31 16:04:29 2021.

Welcome to the ion insertion workflow (iiwPBEU) 
===============================================
The ion insertion workflow (iiwPBEU) intends to automate all steps for 
calculating bulk properties of insertion cathodes like stability (volume change 
and convex hull),voltages (high and low state of charge) as well as kinetic 
properties (migration barriers) using the Nudged Elastic Band method. All 
calculations are carried out in the framework of Density Functional Theory.

What is fully automatic? - The idea is no user intervention, meaning all you
need to run is

::

    $ workflow run my_structure.cif

Wow! Is it really that simple you might think? - Unfortuntely the answer is no,
which does not change the underlying principle implied in automatically running
the calculations without any further user intervention. This also means all your
calculations will be reproducible. All the details on how fully automatic 
calculations are achieved and what assumptions are made can be found in the paper:

.. seealso::

    
    F. T. Bölle, N. R. Mathiesen, A. J. Nielsen, T. Vegge, J. M. Garcia-Lastra, 
    I. E. Castelli, *Autonomous Discovery of Materials for Intercalation Electrodes*, 
    Batteries & Supercaps 2020, 3, 488.
    `http://dx.doi.org/10.1002/batt.201900152 <http://dx.doi.org/10.1002/batt.201900152>`_


Getting started
###############

Given you are familiar with running calculations on a cluster using one of 
SLURM/PBS/LSF through the `MyQueue <https://myqueue.readthedocs.io>`_ fronted as 
well as running your VASP calculations through ASE on the cluster, all you need 
to do is:

::

    $ git clone https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow.git
    $ cd ion-insertion-battery-workflow
    $ pip install -e .

and have a look at how to :ref:`run the workflow <Run the workflow>`.

Otherwise, have a look at the :ref:`installation guide <Installation>` helping you to get started
with MyQueue and ASE.



.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./install
   ./run
   ./wf_structure
   ./modules/modules_index
   



Indices and tables
==================

* :ref:`genindex`
