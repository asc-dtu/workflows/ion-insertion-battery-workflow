# ==============================================================================
#                   RELAX SCALED BULK WITHOUT IONS
# ==============================================================================
import iiwPBEU.vasp_setups as vs
from iiwPBEU.battery_tools import get_starting_structure_from_db,\
    get_scaled_supercell,\
    get_relaxed_empty_from_folder,\
    get_relaxed_structure_from_folder
from iiwPBEU.input import UserInput
from iiwPBEU.tasks_relax import TaskRelax


class TaskRelaxBulkEmptyScaled(TaskRelax):
    """ Relax a bulk structure with scaled cell size """
    _calc_hf_nback = 6  # how far is calculations home folder back
    _mat_hf_nback = 3  # how far back is material home folder

    def __init__(self, task_name='relax_bulk_empty_scaled', *args, **kwargs):
        super(TaskRelaxBulkEmptyScaled, self).__init__(*args, **kwargs)
        self.task_name = task_name

    def run(self):
        """ run all steps """
        self.retrieve_information()
        atoms, hubbard, U_dic = self.get_atoms_object()
        calc = self.get_calculator_object(atoms, hubbard, U_dic)
        status = self.relax(atoms, calc, self.magstate)
        return status

    def retrieve_information(self):
        user_args = UserInput.read_user_input(self._mat_hf_nback)
        self.material_id = user_args["material_id"]
        self.magstate = user_args["magstate"]
        self.ion = user_args["ion"]
        self.with_u_corr = user_args["with_u_corr"]

    def get_atoms_object(self):
        _, hubbard, U_dic = get_starting_structure_from_db(
            gobacknfolders=self._calc_hf_nback,
            material_id=self.material_id,
            with_u_corr=self.with_u_corr)

        # scale atoms according to relaxed emptied unit cell
        uc_filled = get_relaxed_structure_from_folder(
            self._mat_hf_nback, self.magstate)
        uc_empty = get_relaxed_empty_from_folder(
            self._mat_hf_nback, self.magstate)
        atoms = get_scaled_supercell(supercell=uc_filled,
                                     supercell_empty=uc_empty)

        ion_indices = [atom.index for atom in atoms if atom.symbol == self.ion]
        del atoms[ion_indices]

        return atoms, hubbard, U_dic

    def get_calculator_object(self, atoms, hubbard, U_dic):
        # set calculator with isif = 0, do not want to change volume again
        calc = vs.get_vasp_calc_potential_relaxation(atoms,
                                                     hubbard=hubbard,
                                                     U_dic=U_dic)
        return calc

    def relax(self, atoms, calc, magstate):
        # might set istart=1 tag for calculator
        status = vs.relaxation_routine_vasp_bulk(atoms, calc, magstate)
        return status


def relax_bulk_empty_scaled():
    task_bulk = TaskRelaxBulkEmptyScaled()
    status = task_bulk.run()
    return status


if __name__ == "__main__":
    status = relax_bulk_empty_scaled()
