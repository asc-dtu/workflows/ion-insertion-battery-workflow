#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29, 2019

Calculate convex_hull energies for any structure

@author: Felix Tim Boelle <feltbo@dtu.dk>

"""
import re

from ase.phasediagram import PhaseDiagram
from ase.formula import Formula


def mat_str_decompose(mat_str):
    """ returns elements from material string and removes digits"""
    mat_el = re.findall('[A-Z][^A-Z]*', mat_str)

    mat_sym = []
    for el in mat_el:
        mat_sym.append(''.join([i for i in el if i.isalpha()]))
    return mat_sym


def n_unit_material(material):
    '''returns number of elements one unit is made of from formula'''
    mat_el = re.findall('[A-Z][^A-Z]*', material)
    n_at_unit = 0
    for el in mat_el:
        if any([i for i in el if i.isdigit()]):
            n_at_unit += int(''.join([i for i in el if i.isdigit()]))
        else:
            n_at_unit += 1

    return n_at_unit


def formation_energy(red_formula, total_energy, db_ref):
    """ Calculate total formation energy for given structure """
    energy = total_energy
    count = Formula(red_formula).count()

    e0 = 0
    for symbol, n in count.items():
        # O calculated with water reference, calculation at top of script
        if symbol == 'O':
            e_dft_H2 = db_ref.get(db_id='ase.build.molecule.H2').energy
            e_dft_H2O = db_ref.get(db_id='ase.build.molecule.H2O').energy
            e0 += n*get_oxygen_ref_from_water(e_dft_H2, e_dft_H2O)
        else:
            e0 += n*db_ref.get(elements=symbol).energy_per_atom

    return energy - e0


def get_oxygen_ref_from_water(e_dft_H2, e_dft_H2O):
    """ Oxygen reference enery from water reference

    Parameters
    ------------
    e_dft_h2 : float
        DFT total reference energy for H2 meolcule (NOT per atom)
    e_dft_h20 : float
        DFT total reference energy for H2O meolcule (NOT per atom)

    Returns
    --------
    e_dft_O : float
        DFT calculated oxygen reference energy with water reference per atom.

    """
    # ZPE values were taken from Rossmeisel et al. (2005)
    # http://dcwww.camd.dtu.dk/~jross/Welcome_files/Rossmeisl_CP_2005.pdf
    ZPE = {'H2': 0.27, 'H2O': 0.56, '0.5O2': 0.05}

    # formation energies at 0K
    # https://atct.anl.gov/Thermochemical%20Data/version%201.122/species/?species_number=968
    H_0_0K = {'H2O': -2.476}

    # reference DFT energies
    E_DFT = {'H2': e_dft_H2, 'H2O': e_dft_H2O}

    # get oxygen with water reference
    E_DFT_O = E_DFT['H2O'] + ZPE['H2O'] - \
        (E_DFT['H2'] + ZPE['H2'] + H_0_0K['H2O']) - ZPE['0.5O2']

    return E_DFT_O


def get_convex_hull_e(formula, energy_total, db_ref,
                      verbose=False, plot=False, ThreeD=False, dbref=False):
    """ Checks how the material performs on the convex hull

    Parameters
    ----------
    formula : str
        Formula of wanted structure on convex hull (not reduced!)
    f_energy_total : float
        Total heat of formation of the reference element! Not the total energy!
        Need to build reference list for from ase.phasediagram.PhaseDiagram
        extract from ASE documentation:
            List of references.  The energy must be the total energy and not
            energy per atom.  The names can also be dicts like
            ``{'Zn': 1, 'O': 2}`` which would be equivalent to ``'ZnO2'``.
    db_ref :  ase-db object
        Database containing all the reference energies

    Notes
    -------
    Similar to CMR 2D project:
    https://gitlab.com/camd/cmr/blob/master/c2db/convex_hull.py

    """
    material = formula
    n_atoms = n_unit_material(material)

    # get the references for PhaseDiagram
    refs = get_all_total_e_references(db_ref, material)

    # calculate energy on convex hull
    pd = PhaseDiagram(refs, verbose=verbose)

    # return what energy per unit formula is on the convex hull!
    e_form0, _, _ = pd.decompose(formula=material)

    # normalize to per atom energies
    e_ch_per_atom = (energy_total - e_form0)/n_atoms

    # also calculate the heat of formation
    form_energy_total = formation_energy(material, energy_total, db_ref)
    hof_per_atom = form_energy_total/n_atoms

    # plotting if wanted
    if plot:
        import matplotlib.pyplot as plt
        refs.append((material, energy_total))
        pd = PhaseDiagram(refs, verbose=verbose)
        if ThreeD:
            pd.plot(dims=3, show=True)
            # not plt.show() for ubuntu 16.04 for interactive plots apparently
            plt.draw()
        else:
            pd.plot()
            plt.draw()

    return hof_per_atom, e_ch_per_atom


def get_all_total_e_references(db_ref, material):
    """ Get references in total energy and non reduced formulas """
    mat_dec = mat_str_decompose(material)

    refs = []
    for row in db_ref.select():
        mat_db = mat_str_decompose(row.formula)
        if not mat_db:  # empty list
            continue
        if all([el in mat_dec for el in mat_db]):
            if len(re.findall('[A-Z][^A-Z]*', row.formula)) == 1:  # elemental
                el_sym = ''.join([i for i in row.formula if not i.isdigit()])
                # skip oxygen (special reference)
                if el_sym == 'O':
                    continue
                atoms_temp = db_ref.get_atoms(id=row.id)
                el_ref = (atoms_temp.get_chemical_formula(),
                          atoms_temp.get_potential_energy())
                refs.append(el_ref)
            else:  # else it is binary/ternary etc.
                atoms_temp = db_ref.get_atoms(id=row.id)
                st_ref = (atoms_temp.get_chemical_formula(),
                          atoms_temp.get_potential_energy())
                refs.append(st_ref)
    # add any special elements, used water reference for oxygen
    if 'O' in mat_str_decompose(material):
        refs_syms = [e[0] for e in refs]
        if 'O' not in refs_syms:
            e_dft_H2 = db_ref.get(formula='H2').energy
            e_dft_H2O = db_ref.get(formula='H2O').energy
            e_O = get_oxygen_ref_from_water(e_dft_H2, e_dft_H2O)
            refs.append(('O', e_O))

    return refs


def get_ch_hof_energy(atoms, total_energy, db_ref):
    """ run all steps for given structure

    Parameters
    ----------
    atoms : ase-atoms object
    total_energy : float
        Total energy of atoms object. Explicitly given if structure is not
        read in with energy for example.
    db_ref : ase-database object
        Note: All reference are assumed to be unique! There should only be a
        single entry for e.g. the 'H2O' molecule. There also has to be one
        entry called elements with elements=symbol. That way one can extract
        faster reference energies for pure bulk structures.

    Returns
    ---------
    e_ch : float
        Energy on convex hull given the reference energies per atom.
    HoF : float
        Heat of formation for given structure per atom.

    """

    formula = atoms.get_chemical_formula()

    e_form0_per_atom, e_ch_per_atom = get_convex_hull_e(
        formula, total_energy, db_ref)

    return e_form0_per_atom, e_ch_per_atom
