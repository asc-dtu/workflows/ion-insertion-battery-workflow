"""
Script for storing basic descriptor functions for battery materials

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""

from ase.data import atomic_masses, atomic_numbers
from ase.units import create_units

from iiwPBEU.data import n_val_ele, constants


# always use same input
ase_units = create_units('2014')


def get_volume_change(atoms_full, atoms_empty):
    """ Returns the volume change between two structures

    Parameters
    ----------
    atoms_full : ase-atoms object
        Fully discharged phase - all ions in structure
    atoms_empty : ase-atoms object
        Fully charged phase - ion emptied

    Returns
    --------
    v_change : float
        Volume chang in percent

    """
    v_full = atoms_full.get_volume()
    v_empty = atoms_empty.get_volume()
    v_change = ((v_empty - v_full)/v_full) * 100

    return v_change


def get_volume_per_element(atoms, element):
    """
    Returns the volume per element in the cell

    Parameters
    ----------
    atoms : ase - atoms object
    element : string,
        Element of interest.

    Returns
    -------
    V_el : float
        Volume per element.

    Notes
    ------
    Descriptor based on doi:10.1038/s41467-017-01772-1
    Larger ion mobility if larger volume per anion.

    """
    V_cell = atoms.get_volume()
    n_el = atoms.get_chemical_symbols().count(element)

    V_el_cell = V_cell/n_el

    return V_el_cell


def get_mol_weight(atoms):
    """ Returns the molecular weight of an atoms object in [g/mol] """
    Mol_w = 0

    chem_syms = atoms.get_chemical_symbols()
    for el in list(set(chem_syms)):
        Mol_w += atomic_masses[atomic_numbers[el]] * chem_syms.count(el)

    return Mol_w


def get_gravimetric_capacity(atoms, ion):
    """
    Returns the theoretical gravimetric capacity [mAh/g]

    Parameters
    -----------
    atoms : ase-atoms object
    ion : str
        Moving ion in battery

    """
    n_ions = atoms.get_chemical_symbols().count(ion)  # number of ions
    z = n_val_ele[ion]  # number of charge carriers per ion
    F = constants['F']  # Farrady constant [sA/mol]
    Mol_w = get_mol_weight(atoms)  # [g/mol]

    Q_th = ((z * n_ions * F) / (3600 * Mol_w)) * 1000  # [mAh/g]
    return Q_th


def get_volumetric_capacity(atoms, ion):
    """
    Returns the theoretical volumetric capacity [mAh/cm3]

    Parameters
    -----------
    atoms : ase-atoms object
    ion : str
        Moving ion in battery

    """
    n_ions = atoms.get_chemical_symbols().count(ion)  # number of ions
    z = n_val_ele[ion]  # number of charge carriers per ion
    F = constants['F']  # Farrady constant [sA/mol]
    Nav = ase_units['_Nav']
    V = atoms.get_volume()

    Q_th = ((z * n_ions * F) / (V))\
        * (1000 / (3600 * Nav * (1e-8)**3))  # [mAh/cm^3]

    return Q_th
