"""
Created on Thu Dec 2, 2019

Script for checking the stability of a structure for the ion-battery workflow

@author: Felix Tim Boelle <feltbo@dtu.dk>
"""
import json
import glob

from ase.io import read

from iiwPBEU.battery_tools import get_relaxed_structure_from_folder,\
    get_relaxed_empty_from_folder,\
    connect_to_ref_db
from iiwPBEU.convex_hull import get_ch_hof_energy
from iiwPBEU.descriptors.basic import get_volume_change
import iiwPBEU.vasp_setups as vs


def read_from_check_stability_out(path_to_file):
    """ Read output from task """
    path = path_to_file + '/iiwPBEU.check_stability.*'
    full_filename = glob.glob(path)[0].split('/')[-1]  # last entry in path
    path = '/'.join(('/'.join(path.split('/')[:-1]), full_filename))
    e_ch_full = None
    e_ch_empty = None
    with open(path, 'r') as f:
        for line in f:
            if line.rfind('E_CH_Full') > -1:
                e_ch_full = float(line.split()[1])
            if line.rfind('E_CH_empty') > -1:
                e_ch_empty = float(line.split()[1])

    return e_ch_full, e_ch_empty


def return_e_from_path(path):
    """ Return energy of atoms given absolute path"""
    atoms_f = vs.get_atoms_filename()
    atoms = read('/'.join((path, atoms_f)))
    e = vars(atoms.calc)['results']['energy']
    return e


def check_stability():
    # reference database for ch energies
    db_ref = connect_to_ref_db(gobacknfolders=3)

    # get information on calculation
    with open('./current_material.json') as f:
        user_args = json.load(f)
    magstate = user_args['magstate']

    # get remove_n_ions state
    remove_n_ions = user_args.get('remove_n_ions')

    # threshold values
    v_change_threshold = user_args['thresholds']['volume_change']  # in (%)
    e_ch_threshold = user_args['thresholds']['ch_energy']  # in (eV)

    # get the structures
    uc_full, p_full = get_relaxed_structure_from_folder(
        0, magstate, return_path=True)

    # check convex hull stabilities
    total_e_full = return_e_from_path(p_full)
    _, e_ch_full = get_ch_hof_energy(uc_full, total_e_full, db_ref)
    print("E_CH_Full:", e_ch_full)

    if e_ch_full > e_ch_threshold and remove_n_ions == "single_one":
        msg = ("Discharged cell exceeds ch criterion"
               f" {e_ch_full}/{e_ch_threshold} (eV/atom)")
        raise ValueError(msg)

    if not remove_n_ions == 'single_one':
        # also check empty cell and volume changes
        uc_empty, p_empty = get_relaxed_empty_from_folder(
            0, magstate, return_path=True)

        # check volume change
        v_change = get_volume_change(uc_full, uc_empty)
        if abs(v_change) > v_change_threshold:
            msg = (f"Volume change {v_change} % exceeds threshold"
                   f" {v_change_threshold} %")
            raise ValueError(msg)

        total_e_empty = return_e_from_path(p_empty)
        _, e_ch_empty = get_ch_hof_energy(uc_empty, total_e_empty, db_ref)

        print("E_CH_empty:", e_ch_empty)
        print("volume change is:", v_change)
        if e_ch_empty > e_ch_threshold:
            msg = ("Charged cell exceeds ch criterion"
                   f" {e_ch_empty}/{e_ch_threshold} (eV/atom)")
            print(msg)

        if e_ch_full > e_ch_threshold and e_ch_empty > e_ch_threshold:
            msg = "Convex hull criterion exceeded for both charge states"
            raise ValueError(msg)

    return e_ch_full, e_ch_empty, v_change


if __name__ == "__main__":
    e_ch_full, e_ch_empty, v_change = check_stability()
