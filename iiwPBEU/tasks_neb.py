"""
Created on Thu Mar 12 17:15:02 2020

Class for storing all NEB tasks

@author: Felix Bölle <feltbo@dtu.dk>
"""
import os
import json
import copy
from shutil import copy2
import numpy as np
import logging

from ase.io import write, read
from ase.calculators.emt import EMT
from ase.optimize import FIRE, BFGS, MDMin
from ase.calculators.singlepoint import SinglePointCalculator
from ase.neb import NEB

from iiwPBEU.system_utils import get_wd, delete_files_cwd, touch, simple_cd
from iiwPBEU.neb.neb_path import NEBPath
from iiwPBEU.battery_tools import get_relaxed_potential_structure_from_folder,\
    get_required_unrelaxed_structures,\
    get_supercell_from_folder,\
    get_relaxed_empty_from_folder,\
    get_scaled_supercell,\
    get_correct_supercell_for_symmetry,\
    get_min_bar
import iiwPBEU.vasp_setups as vs
from iiwPBEU.fix_dft_calculation import FixVaspCalculation, FixVaspError
from iiwPBEU.vasp_setups import MyVaspError


class BaseTaskNEB:
    """ Base Task for all NEB tasks """
    _hf_material_nback = 5  # how far back is material home folder
    _sym_tol = 1e-3  # default symmetry tolerance for reflection
    _fmax_pre = 0.12  # force criterion for pre convergence (FIRE -> BFGS)
    _fmax = 0.05  # force criterion for convergence
    _switch_opt_f = 'SET_OPT_BFGS'  # indicate switching optimizer

    def __init__(self, init_p_name='initial_path.traj'):
        self.init_p_name = init_p_name
        self.N = self.get_N_images()

    def get_images(self):
        """ Get the starting path """

    def retrieve_information(self):
        """ info from folder structure and json files """
        # retrieve information from folder structure
        folder_info = os.getcwd().split('/')
        self.magstate = folder_info[-2]
        init_ind, final_ind, remove_n_ions, N_ions = folder_info[-4].split('_')

        self.init_ind = int(init_ind)
        self.final_ind = int(final_ind)
        self.remove_n_ions = int(remove_n_ions)
        self.N_ions = int(N_ions)

        with open('symmetry.json') as f:
            self.sym_dic = json.load(f)

        potneb_f = './' + '../'*self._hf_material_nback\
                   + 'potneb_dictionary.json'
        with open(potneb_f) as f:
            self.potneb_dic = json.load(f)

    def relax(self):
        """ Call optimizer and relax path """

    def launch_NEB(self,
                   N=5,
                   magstate='NM',
                   RNEB=True,
                   RMI_NEB=False,
                   EMT_test=False,
                   tol=1e-3,
                   ncore=None,
                   init_path=None,
                   climb=False,
                   CIRNEB=False,
                   max_restart=3):
        """ Wrap around fix_dft_calculation for automatic restart """
        n_restarts = 0
        for n_restarts in range(max_restart):
            try:
                images = self._launch_NEB(
                    N=N, magstate=magstate,
                    RNEB=RNEB, RMI_NEB=RMI_NEB, EMT_test=EMT_test,
                    tol=tol, ncore=ncore, init_path=init_path,
                    climb=climb, CIRNEB=CIRNEB)
                return images
            except FixVaspError as e:
                raise FixVaspError(e.message)
            except MyVaspError as e:
                raise MyVaspError(e.message)
            except BaseException as e:
                self.setup_error_log()
                self.logger.error(e, exc_info=True)
                n_restarts += 1

        msg = ("Maximum number of routine restarts reached"
               f" {n_restarts}/{max_restart}")
        raise ValueError(msg)

    def setup_error_log(self):
        """ Error log with traceback """
        logging.basicConfig(filemode='a')
        self.logger = logging.getLogger(__name__)
        fl = logging.FileHandler("NEB_failed.log")
        if not len(self.logger.handlers):
            self.logger.addHandler(fl)

    def _launch_NEB(self, N=5, magstate='NM', RNEB=False, RMI_NEB=False,
                    EMT_test=False, tol=1e-3, ncore=None, init_path=None,
                    climb=False, CIRNEB=False):
        """
        Start the NEB calculation. Chooses either regular NEB, R-NEB, or RMI-NEB

        Parameters
        ----------

        N: integer
        Number of images

        magstate: string?
        the magnetic ordering

        RNEB: boolean
        whether or not to run an R-NEB calculation
        (if reflection symmetry is present)

        RMI-NEB: boolean
        whether or not to run an RMI-NEB calculation
        (if reflection symmetry is present)

        tol: float
        tolerance on the atomic position during symmetry analysis
        """
        sym, S = self.get_symmetry()
        images = read(self.init_p_name + '@:')
        calc = self.get_calculator_object(images[0])

        # restart of NEB if needed
        images, calc = self.automatic_error_handler(images, calc)

        if RMI_NEB and sym:
            assert len(images) == 3  # RMI always has only three images
        else:
            assert len(images) == N

        self.setup_calculator_images(images,
                                     t=N-1,
                                     calc=calc,
                                     magstate=self.magstate,
                                     init_path=init_path,
                                     EMT_test=EMT_test)

        images = self.relax(images)

        return images

    @staticmethod
    def automatic_error_handler(images, calc):
        """ return changed atoms/calc object if wanted """
        fixvasp = FixVaspCalculation(atoms=images, calculator=calc)
        images, calc = fixvasp.fix_if_needed_NEB()
        return images, calc

    @staticmethod
    def get_calculator_object(image):
        """ Returns an ase calculator object """
        # only the cell of the atoms object is used for getting calc object!
        return vs.get_vasp_calc_NEB(image)

    def update_calculator_images(self, images, params_dic={'ediff': 1e-6}):
        """ Update already initialized calculator images """
        t = self.N - 1
        for image in images[1:t]:
            image.calc.set(**params_dic)
        return images

    def setup_calculator_images(self, images, t, calc,
                                magstate='NM', EMT_test=False, init_path='.'):
        """ Initialize the calculator on the images """
        cwd = os.getcwd()
        for i, image in enumerate(images[1:t]):
            if EMT_test:
                calc_copy = EMT()
            # set magnetic moments
            else:
                # separate calc object for each image
                calc_copy = copy.deepcopy(calc)
                image, calc_copy = self.setup_magmoms_NEB(
                    image, calc_copy, magstate, i, init_path)
                # run each image in different folder
                calc_copy.set(directory=cwd + f'/image_{i}')
            image.set_calculator(calc_copy)

    def setup_magmoms_NEB(self, image, calc, magstate, i, init_path):
        """ Specific magnetic moment setup for NEB

        Parameters
        ---------------
        i : int
            Image number of NEB path

        init_path : string
            path to folder containing inital image calculation
        """

        if magstate == 'NM':
            image, calc = vs.setup_magnetic_moments_vasp(image, calc, magstate)
        elif magstate == 'FM':
            # first create calculation directory
            calc_dir = f'image_{i}'
            full_calc_dir = get_wd([calc_dir])
            mu_total = self.initialize_image_from_folder(
                init_path, full_calc_dir, i)

            # fix total magnetic moment to moment of reference calculation
            # if not wanted file needs to be there
            if os.path.isfile(os.getcwd() + '/SET_CALC_NUPDOWN_FALSE'):
                calc.set(isym=1)
            else:
                calc.set(nupdown=int(round(mu_total)))
            # initalize moments from WAVECAR, VASP pushes to mu_total_is
            calc.set(istart=1)
            calc.set(ispin=2)
            calc.set(lorbit=11)

        return image, calc

    def initialize_image_from_folder(self, folder_calc, full_calc_dir, i):
        """ Initialize image from existing calculation """
        # start copying initial state WAVECAR into calculation directory
        # only copy in there if not already present (restarted calculation)
        if not os.path.isfile(full_calc_dir + '/WAVECAR'):
            copy2(folder_calc + '/WAVECAR', full_calc_dir)

        # fix total magnetic moment with moment from IS
        mu_total = vs.get_magnetization_OUTCAR(folder_calc + '/OUTCAR')

        return mu_total

    def get_results_dic_from_path(self, path, atoms_f=None):
        """ Return results dic from calculation """
        if not atoms_f:
            atoms_f = vs.get_atoms_filename()
        atoms_init = read(path + f"/{atoms_f}")
        results = atoms_init.calc.results
        return results

    def get_symmetry(self):
        with open("symmetry.json") as f:
            res = json.load(f)
        sym = res['symmetric']
        S = res['symmetries']
        return sym, S

    def get_N_images(self, N=5):
        """ return the number of images """
        return N

    def get_optimizer(self, neb, choice='FIRE', traj_name='neb.traj'):
        """ Return optimizer """
        if choice == 'FIRE':
            opt = FIRE(neb, trajectory=traj_name, logfile='FIRE.log')

        if choice == 'MDMin':
            opt = MDMin(neb, trajectory=traj_name, logfile='MDMin.log')

        # always choose BFGS if file is there
        if os.path.isfile(self._switch_opt_f) or choice == 'BFGS':
            touch(self._switch_opt_f)
            opt = BFGS(neb, trajectory=traj_name, logfile='BFGS.log')

        return opt

    def write_initial_path(self, images):
        write(self.init_p_name, images)

    def save_diskspace(self):
        """ Remove files to save diskpace after calc finished """
        # clean all WAVECAR files to save storage
        delete_files_cwd(filename='WAVECAR')


class TaskRMINEB(BaseTaskNEB):
    """ Implement the ReflectiveMiddleImage-NEB (RMI-NEB) """

    def __init__(self, task_name=None, *args, **kwargs):
        super(TaskRMINEB, self).__init__(*args, **kwargs)
        self.task_name = 'RMINEB'

    def run(self):
        """ Run all the steps needed """
        self.retrieve_information()
        init = self.retrieve_initial_relaxed_image()
        _ = self.get_images(init)
        _ = self.launch_NEB(N=self.N,
                            magstate=self.magstate,
                            RMI_NEB=True,
                            tol=self._sym_tol,
                            init_path=self.init_path)
        self.save_diskspace()

    def relax(self, images):
        neb = NEB(images)
        if not os.path.isfile(self._switch_opt_f):
            # pre converge with FIRE
            opt = self.get_optimizer(neb, choice='MDMin')
            opt.run(fmax=self._fmax_pre)
            copy2('neb.traj', 'neb_MDMin_done.traj')  # no append traj in opt?
        # switch to BFGS after reaching threshold and lower ediff
        images = self.update_calculator_images(images)
        neb = NEB(images)
        opt = self.get_optimizer(neb, choice='BFGS')
        opt.run(fmax=self._fmax)
        return images

    def retrieve_initial_relaxed_image(self):
        """ get the initial relaxed image """
        # path to init structure for copying WAVECARS in NEB
        atoms_init, init_path = get_relaxed_potential_structure_from_folder(
            self._hf_material_nback,
            n_mg=self.remove_n_ions,
            N_mg=self.N_ions,
            unique_ind=self.init_ind,
            magstate=self.magstate,
            return_path=True)
        self.init_path = init_path
        return atoms_init

    def get_images(self, init, atoms_f=None, restart_f='ase-sort_0.dat'):
        """ Return path created from relaxed init, unrelaxed init0,final0 """
        supercell = get_supercell_from_folder(self._hf_material_nback)

        # if remove_n_ions = 'single_one' than there is no supercell_empty
        supercell_empty = None
        if self.remove_n_ions != 1:
            empty_uc = get_relaxed_empty_from_folder(self._hf_material_nback,
                                                     self.magstate,
                                                     atoms_f=atoms_f,
                                                     restart_f=restart_f)
            supercell_empty = empty_uc.repeat(
                self.potneb_dic['repeat_unitcell'])

        supercell_scaled = get_scaled_supercell(supercell, supercell_empty)

        init0, final0 = get_required_unrelaxed_structures(
            supercell, self.init_ind, self.final_ind, self.remove_n_ions,
            self.N_ions, supercell_empty=supercell_empty)

        # choose correct supercell for charge state
        sc_temp = get_correct_supercell_for_symmetry(
            self.remove_n_ions, supercell, supercell_scaled)

        # create the path
        # RMI-NEB has always only three images
        self.N = 3
        neb_path = NEBPath(tol=self._sym_tol)
        images = neb_path.get_neb_path(
            init=init,  # relaxed initial defect structure
            N=self.N,  # number of images
            init0=init0,  # unrelaxed initial defect structure
            final0=final0,  # unrelaxed final defect structure
            orig=sc_temp)  # relaxed supercell

        self.write_initial_path(images)
        return images


class TaskCIRNEB(BaseTaskNEB):
    """ Implement the ClimbingImageReflective-NEB (CIR-NEB) """

    ts_found_msg = "Transition state already found using RMINEB"

    def __init__(self, barrier_threshold=2, *args, **kwargs):
        super(TaskCIRNEB, self).__init__(*args, **kwargs)
        self.task_name = 'CIRNEB'
        self.barrier_threshold = barrier_threshold  # eV

    def run(self):
        """ Run all the steps needed """
        self.copy_symmetry_dic()
        self.retrieve_information()
        _ = self.retrieve_initial_relaxed_image()  # needed for threshold

        self.check_reflective_true()
        rmi_images = self.check_diffusivity_threshold()

        _ = self.get_images(rmi_images)
        _ = self.launch_NEB(N=self.N,
                            magstate=self.magstate,
                            tol=self._sym_tol,
                            init_path=self.init_path)

        self.save_diskspace()

    def relax(self, images, climb=True):
        neb = NEB(images, climb=climb)
        ts_found = False

        if not os.path.isfile(self._switch_opt_f):
            opt = self.get_optimizer(neb, choice='MDMin')
            # check if below RMI-NEB barrier after every ionic step
            for conv in opt.irun(fmax=self._fmax_pre):
                ts_found = self.check_images_below_is_fs(images)
                if ts_found:
                    print(self.ts_found_msg)
                    write('neb.traj', images)
                    break  # no return - need to finish irun loop for es

            copy2('neb.traj', 'neb_MDMin_done.traj')
            if ts_found:
                return images

        # switch to BFGS after reaching threshold
        images = self.update_calculator_images(images)
        neb = NEB(images, climb=climb)
        opt = self.get_optimizer(neb, choice='BFGS')
        # check if below RMI-NEB barrier after every ionic step
        for conv in opt.irun(fmax=self._fmax):
            ts_found = self.check_images_below_is_fs(images)
            if ts_found:
                print(self.ts_found_msg)
                write('neb.traj', images)
                break

        return images

    def copy_symmetry_dic(self):
        """ Copy symmetry.json from RMINEB into folder - same path """
        functional = vs.get_functional()
        folder_info = os.getcwd().split('/')
        self.magstate = folder_info[-2]
        path_neb = f'./../../../RMINEB/{self.magstate}/vasp_rx_{functional}/'
        copy2(path_neb + 'symmetry.json', '.')

    def retrieve_initial_relaxed_image(self):
        """ get the initial relaxed image """
        # path to init structure for copying WAVECARS in NEB
        atoms_init, init_path = get_relaxed_potential_structure_from_folder(
            self._hf_material_nback,
            n_mg=self.remove_n_ions,
            N_mg=self.N_ions,
            unique_ind=self.init_ind,
            magstate=self.magstate,
            return_path=True)
        self.init_path = init_path
        return atoms_init

    def check_diffusivity_threshold(self):
        """ Check if RMI-NEB barrier already above threshold """
        # get RMI-NEB trajectory
        functional = vs.get_functional()
        path_neb = f'./../../../RMINEB/{self.magstate}/vasp_rx_{functional}/'
        rmi_images = read(path_neb + 'neb.traj@-3:')  # only three images
        results_init = self.get_results_dic_from_path(self.init_path)
        results = {'energy': results_init['energy']}
        # set energy on initial and final
        rmi_images[0].set_calculator(SinglePointCalculator(
            rmi_images[0], **results))
        rmi_images[-1].set_calculator(SinglePointCalculator(
            rmi_images[-1], **results))

        barrier = get_min_bar(rmi_images)
        if barrier > self.barrier_threshold:
            msg = (f"Barrier {barrier} eV exceeded threshold of"
                   f" {self.barrier_threshold} eV")
            raise ValueError(msg)
        return rmi_images

    def check_reflective_true(self):
        """ Already checked for the RMI-NEB - needed if run separately"""
        return True

    def check_images_below_is_fs(self, images):
        """ Check if all images fall below energies of IS or FS """
        N = self.N
        e_is = images[0].calc.results['energy']
        e_fs = images[-1].calc.results['energy']
        es_images = np.array([im.calc.results['energy']
                             for im in images[1:N-1]])

        if np.all(es_images - e_is < 0) or np.all(es_images - e_fs < 0):
            return True
        return False

    def get_images(self, rmi_images):
        """ Return path created from relaxed init and MiddleImage of RMINEB"""
        init = rmi_images[0]
        # that is the middle image, set as final for CI-RNEB
        final = rmi_images[1]
        results_final = final.calc.results
        final.set_calculator(SinglePointCalculator(final, **results_final))
        # path known to be reflective, final relaxed exists
        # indices assumed to be ALIGNED since reading ASE trajectory!
        neb_path = NEBPath(tol=self._sym_tol)
        images = neb_path.create_path(init, final, self.N)

        self.write_initial_path(images)

        return images


class TaskCNEB(BaseTaskNEB):
    """ Implement the conventional-NEB (C-NEB) """

    def __init__(self, task_name=None, barrier_threshold=2, *args, **kwargs):
        super(TaskCNEB, self).__init__(*args, **kwargs)
        self.task_name = 'CNEB'

    def get_N_images(self, N=9):
        """ return the number of images """
        return N

    def run(self):
        """ Run all the steps needed """
        self.retrieve_information()

        _ = self.get_images()
        _ = self.launch_NEB(N=self.N,
                            magstate=self.magstate,
                            RNEB=False,
                            RMI_NEB=False,
                            tol=self._sym_tol,
                            init_path=self.init_path)
        # no WAVECAR removal, need it for CICNEB

    def relax(self, images):
        neb = NEB(images)
        if not os.path.isfile(self._switch_opt_f):
            # pre converge with FIRE
            opt = self.get_optimizer(neb, choice='MDMin')
            opt.run(fmax=self._fmax_pre)
            copy2('neb.traj', 'neb_MDMin_done.traj')  # no append traj in FIRE?
        # switch to BFGS after reaching threshold
        images = self.update_calculator_images(images)
        neb = NEB(images)
        opt = self.get_optimizer(neb, choice='BFGS')
        opt.run(fmax=self._fmax)
        return images

    def initialize_image_from_folder(self, folder_calc, full_calc_dir, i):
        """ Initialize image from existing calculation """
        # initial and final symmetry equivalent?
        if self.init_path == self.final_path:
            # start copying initial state WAVECAR into calculation directory
            # only copy in there if not already present (restarted calculation)
            if not os.path.isfile(full_calc_dir + '/WAVECAR'):
                copy2(folder_calc + '/WAVECAR', full_calc_dir)

            # fix total magnetic moment with moment from IS
            mu_total = vs.get_magnetization_OUTCAR(folder_calc + '/OUTCAR')

        else:
            # interpolate in between initial and final image
            if i + 1 <= self.N // 2:
                if not os.path.isfile(full_calc_dir + '/WAVECAR'):
                    copy2(self.init_path + '/WAVECAR', full_calc_dir)
            else:
                if not os.path.isfile(full_calc_dir + '/WAVECAR'):
                    copy2(self.final_path + '/WAVECAR', full_calc_dir)

            # now the total magnetic moment interpolated from IS/FS
            mu_total_is = vs.get_magnetization_OUTCAR(self.init_path+'/OUTCAR')
            mu_total_fs = vs.get_magnetization_OUTCAR(self.final_path+'/OUTCAR')
            mu_total = mu_total_is + \
                ((i + 1)/(self.N - 1)) * (mu_total_fs - mu_total_is)

            # absolute change more than 10 for FS/IS
            mu_change_thr = 10
            if abs(mu_total_is - mu_total_fs) > mu_change_thr:
                msg = (f"Total magnetic moment difference IS/FS ({mu_total_is}"
                       f"/{mu_total_fs}) larger than {mu_change_thr}")
                raise MyVaspError(msg)

        return mu_total

    def get_images(self, atoms_f=None, restart_f='ase-sort_0.dat'):
        """ Return path created from relaxed init and MiddleImage of RMINEB"""
        supercell = get_supercell_from_folder(self._hf_material_nback)

        supercell_empty = None
        if self.remove_n_ions != 1:
            empty_uc = get_relaxed_empty_from_folder(self._hf_material_nback,
                                                     self.magstate,
                                                     atoms_f=atoms_f,
                                                     restart_f=restart_f)
            supercell_empty = empty_uc.repeat(
                self.potneb_dic['repeat_unitcell'])

        supercell_scaled = get_scaled_supercell(supercell, supercell_empty)

        init0, final0 = get_required_unrelaxed_structures(
            supercell, self.init_ind, self.final_ind, self.remove_n_ions,
            self.N_ions, supercell_empty=supercell_empty)

        # create the path
        neb_path = NEBPath(tol=self._sym_tol)
        # path non-reflective, create/get all structures
        init, init0, init_path = self.get_initial_relaxed_image(
            self.init_ind, neb_path, init0, final0,
            supercell_scaled, supercell,
            atoms_f=atoms_f, restart_f=restart_f)
        self.init_path = init_path
        final, final0, final_path = self.get_initial_relaxed_image(
            self.final_ind, neb_path, init0, final0,
            supercell_scaled, supercell,
            atoms_f=atoms_f, restart_f=restart_f)
        self.final_path = final_path

        # choose correct supercell for charge state
        sc_temp = get_correct_supercell_for_symmetry(
            self.remove_n_ions, supercell, supercell_scaled)

        images = neb_path.get_neb_path(
            init=init,  # relaxed initial defect structure
            N=self.N,  # number of images
            final=final,  # relaxed final defect structure
            init0=init0,  # unrelaxed initial defect structure
            final0=final0,  # unrelaxed final defect structure
            orig=sc_temp)  # supercell

        # assign energies to initial and final state
        # not all since forces might be rotated!
        results_init = self.get_results_dic_from_path(self.init_path,
                                                      atoms_f=atoms_f)
        # set energy on initial and final
        images[0].set_calculator(SinglePointCalculator(
            images[0], **{'energy': results_init['energy']}))

        results_final = self.get_results_dic_from_path(self.final_path,
                                                       atoms_f=atoms_f)
        # set energy on initial and final
        images[-1].set_calculator(SinglePointCalculator(
            images[-1], **{'energy': results_final['energy']}))

        self.write_initial_path(images)
        return images

    def get_initial_relaxed_image(self, unique_ind, neb_path, init0, final0,
                                  supercell_scaled, supercell,
                                  atoms_f, restart_f):
        """ Get folder from indice - otherwise create from symmetry """
        try:
            init, init_path = get_relaxed_potential_structure_from_folder(
                self._hf_material_nback,
                n_mg=self.remove_n_ions,
                N_mg=self.N_ions,
                unique_ind=unique_ind,
                magstate=self.magstate,
                restart_f=restart_f, atoms_f=atoms_f, return_path=True)
            if unique_ind == self.final_ind:
                init0 = final0
        except FileNotFoundError:
            # that is the case the indice does not match with the relaxed
            # structure, that could happen if it is symmetry equivalent
            # to the given indice, now need to create this structure from
            # the symmetry equivalent relaxed structure
            init, init0, init_path = self.get_final_from_symmetry(
                neb_path, unique_ind, init0, final0, supercell_scaled,
                supercell, atoms_f, restart_f)

        return init, init0, init_path

    def get_final_from_symmetry(self, neb_path, unique_ind, init0, final0,
                                supercell_scaled, supercell=None,
                                atoms_f=None, restart_f='ase-sort_0.dat'):
        """ If final relaxed is not there with wanted indice but a symmetrical
        equivalent structure is apparent with different indice, this function
        will create the final structure from symmetry considerations

        Parameters
        -------------
        supercell [optional] : ase.atoms object
            If supplied will not be changed, just to figure out scaled
            positions of ion sites
        """
        # first what is equivalent relaxed ion index? InequivalentSiteFinder
        pair_list = list(zip(self.potneb_dic['all_ion_indices'],
                             self.potneb_dic['equal_to']))
        eq_ind = [pair[1] for pair in pair_list if pair[0] == unique_ind][0]
        init, init_path = get_relaxed_potential_structure_from_folder(
            self._hf_material_nback,
            n_mg=self.remove_n_ions,
            N_mg=self.N_ions,
            unique_ind=eq_ind,
            magstate=self.magstate,
            atoms_f=atoms_f,
            restart_f=restart_f,
            return_path=True)

        # choose correct supercell for charge state
        sc_temp = get_correct_supercell_for_symmetry(
            self.remove_n_ions, supercell, supercell_scaled)

        final, final0 = neb_path.get_neb_path(
            init=init,  # relaxed initial defect structure
            N=self.N,  # number of images
            init0=init0,  # unrelaxed initial defect structure
            final0=final0,  # unrelaxed final defect structure
            orig=sc_temp,  # supercell for RNEB
            get_final=True)
        # now that is what needed to be created so set return the structures
        # Both structures are returned, since get_neb_path ALIGN indices!!!
        # important to keep it like that
        return final, final0, init_path


class TaskCICNEB(TaskCNEB):
    """ Implement the climbing image conventional-NEB (CIC-NEB) """

    def __init__(self, task_name=None, *args, **kwargs):
        super(TaskCICNEB, self).__init__(*args, **kwargs)
        self.task_name = 'CICNEB'

    def run(self):
        """ Run all the steps needed """
        self.copy_symmetry_dic()
        self.retrieve_information()

        _ = self.get_images()
        _ = self.launch_NEB(N=self.N,
                            magstate=self.magstate,
                            RNEB=False,
                            RMI_NEB=False,
                            tol=self._sym_tol,
                            init_path=self.init_path)

        self.save_diskspace()

    def relax(self, images, climb=True):
        # BFGS since pre-converged with CNEB
        images = self.update_calculator_images(images)
        neb = NEB(images, climb=climb)
        opt = self.get_optimizer(neb, choice='BFGS')
        opt.run(fmax=self._fmax)
        return images

    def save_diskspace(self):
        """ Remove files to save diskspace after calc finished """
        # clean all WAVECAR files to save storage, also CNEB ones
        with simple_cd('./../../../'):
            delete_files_cwd(filename='WAVECAR')

    def copy_symmetry_dic(self):
        """ Copy symmetry.json from CNEB into folder """
        functional = vs.get_functional()
        folder_info = os.getcwd().split('/')
        self.magstate = folder_info[-2]
        path_neb = f'./../../../CNEB/{self.magstate}/vasp_rx_{functional}/'
        copy2(path_neb + 'symmetry.json', '.')
        self.r_path_cneb = path_neb

    def get_images(self, atoms_f=None, restart_f='ase-sort_0.dat'):
        super().get_images(atoms_f=atoms_f, restart_f=restart_f)
        cneb_images = read(self.r_path_cneb + f'neb.traj@-{self.N}:')
        self.write_initial_path(cneb_images)
        return cneb_images

    def initialize_image_from_folder(self, folder_calc, full_calc_dir, i):
        """ Initialize image from existing calculation """
        # copy WAVECAR from CNEB task
        f_image = folder_calc.split('/')[-1]
        dir_cneb_image = self.r_path_cneb + "/" + f_image
        if not os.path.isfile(full_calc_dir + '/WAVECAR'):
            copy2(dir_cneb_image + '/WAVECAR', full_calc_dir)

        if self.init_path == self.final_path:
            # fix total magnetic moment with moment from IS
            mu_total = vs.get_magnetization_OUTCAR(folder_calc + '/OUTCAR')
        else:
            # now the total magnetic moment interpolated from IS/FS
            mu_total_is = vs.get_magnetization_OUTCAR(self.init_path+'/OUTCAR')
            mu_total_fs = vs.get_magnetization_OUTCAR(self.final_path+'/OUTCAR')
            mu_total = mu_total_is + \
                ((i + 1)/(self.N - 1)) * (mu_total_fs - mu_total_is)

            # absolute change more than 10 for FS/IS
            mu_change_thr = 10
            if abs(mu_total_is - mu_total_fs) > mu_change_thr:
                msg = (f"Total magnetic moment difference IS/FS ({mu_total_is}"
                       f"/{mu_total_fs}) larger than {mu_change_thr}")
                raise MyVaspError(msg)

        return mu_total
