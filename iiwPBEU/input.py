"""
Created on Thu Apr 22 13:58:44 2021

User and Folder structure input

@author: Felix Bölle <feltbo@dtu.dk>
"""
import os
import shutil
import json
from jsonschema import validate

from ase.db import connect

from iiwPBEU.system_utils import cd, get_wd
import iiwPBEU.vasp_setups as vs
from iiwPBEU.battery_tools import get_starting_structure_from_db
from iiwPBEU.convex_hull import mat_str_decompose


class UserInput:

    f_name = "current_material.json"

    schema_user_input = {
        "type": "object",
        "properties": {
            "material_id":  {"type": "string"},
            "magstate":     {"type": "string"},
            "ion":          {"type": "string"},
            "cut_off_neb":  {"type": "number"},
            "with_u_corr":  {"type": "boolean"},
            "thresholds":   {"type": "object"},
            "remove_n_ions": {"type": "string",
                              "enum": ["all_but_one",
                                       "single_one",
                                       "both_charge_states"]}
        },
        "required": ["material_id",
                     "magstate",
                     "ion",
                     "cut_off_neb",
                     "with_u_corr",
                     "thresholds"]
    }

    schema_thresholds = {
        "type": "object",
        "properties": {
            "ch_energy":      {"type": "number"},
            "potential":      {"type": "number"},
            "diffusivity":    {"type": "number"},
            "volume_change":  {"type": "number"},
        },
        "required": ["ch_energy",
                     "potential",
                     "diffusivity",
                     "volume_change",
                     ]
    }

    def __init__(self, wf_input={}, gobacknfolders=0):
        self.wf_input = wf_input
        self.gobacknfolders = gobacknfolders

    def validate(self):
        """ Verify User input one-by-one """
        self.validate_user_keys()
        self.validate_db_setup()

    @staticmethod
    def read_user_input(gobacknfolders):
        """ Return user arguments parsed to workflow """
        file = './' + '../'*gobacknfolders + UserInput.f_name
        with open(file) as f:
            user_args = json.load(f)
        return user_args

    def validate_user_keys(self):
        # general expected keys and data types
        validate(self.wf_input, self.schema_user_input)
        validate(self.wf_input["thresholds"], self.schema_thresholds)
        self.only_valid_keys(self.wf_input, self.schema_user_input)

        # specific checks for certain strings
        self.validate_material_id(self.wf_input["material_id"])

    @staticmethod
    def only_valid_keys(wf_input, schema_user_input):
        for key in wf_input.keys():
            if key not in schema_user_input["properties"].keys():
                raise KeyError(f"Invalid user input key: '{key}'")

    @staticmethod
    def validate_material_id(material_id):
        web_characters = "\\~'#%&*:<>?/|{}.@"
        if any([char in web_characters for char in material_id]):
            msg = f"""
                Invalid material id: '{material_id}'
                None of these characters are allowed: '{web_characters}' """
            raise NameError(msg)

    def validate_db_setup(self):
        for db in FolderStructure.expected_starting_dbs:
            try:
                assert os.path.isfile(
                    './' + '/'.join((
                        FolderStructure.starting_structures, db)))
            except AssertionError:
                msg = f"""Database '{db}' not found
                    Expected it in : ./{FolderStructure.starting_structures}/
                    """
                raise FileNotFoundError(msg)
        self.validate_structure_in_db()
        self.validate_ch_references()

    def validate_structure_in_db(self):
        get_starting_structure_from_db(
            self.gobacknfolders,
            self.wf_input["material_id"],
            with_u_corr=self.wf_input["with_u_corr"])

    def validate_ch_references(self):
        path_material_db = './' + '/'.join((
            FolderStructure.starting_structures,
            FolderStructure.material_db))
        path_references_db = './' + '/'.join((
            FolderStructure.starting_structures,
            FolderStructure.ch_db))
        db_mat = connect(path_material_db)
        db_refs = connect(path_references_db)

        formula = db_mat.get(
            material_id=self.wf_input["material_id"]).formula
        for element in mat_str_decompose(formula):
            try:
                _ = db_refs.get(elements=element)
            except KeyError:
                msg = f"Missing entry for elements='{element}' in reference db"
                raise KeyError(msg)


class FolderStructure:
    starting_structures = 'starting_structures'
    ch_db = "references_ch.db"
    material_db = "materials.db"
    expected_starting_dbs = [ch_db, material_db]

    def __init__(self, wf_input={}):
        self.wf_input = wf_input
        self.functional = vs.get_functional()
        self.material_home = ['calculations',
                              wf_input["ion"],
                              wf_input["material_id"]]
        self.bulk_rx = self.material_home\
            + ['bulk',
                wf_input["magstate"],
                f'vasp_rx_{self.functional}_U']
        self.empty_bulk_rx = self.material_home\
            + ['bulk_empty',
               wf_input["magstate"],
               f'vasp_rx_{self.functional}_U']
        self.empty_bulk_scaled_rx = self.material_home\
            + ['bulk_empty_scaled',
               wf_input["magstate"],
               f'vasp_rx_{self.functional}_U']

    def mkdirs_wf_start(self):
        material_home_folder = get_wd(self.material_home)
        bulk_rx = get_wd(self.bulk_rx)
        empty_bulk_rx = get_wd(self.empty_bulk_rx)
        empty_bulk_scaled_rx = get_wd(self.empty_bulk_scaled_rx)

        self.mv_user_input(material_home_folder)

        return [material_home_folder,
                bulk_rx,
                empty_bulk_rx,
                empty_bulk_scaled_rx]

    def mv_user_input(self, material_home_folder):
        # copy workflow_potneb.py into materials_home folder
        shutil.copy('workflow_potneb.py',
                    './' + '/'.join(self.material_home) + '/workflow_potneb.py')
        # also copy user input to material_home_folder
        with cd(material_home_folder):
            with open('current_material.json', 'w') as f:
                json.dump(self.wf_input, f, indent=4)
