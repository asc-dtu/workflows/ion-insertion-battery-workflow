#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 11:14:20 2020

Similar to ase.data - stores data I need additionally

@author: Felix Tim Bölle <feltbo@dtu.dk>
"""

# number of valence electrons for electron transfer
n_val_ele = {
    'Li': 1, 'Be': 2,
    'Na': 1, 'Mg': 2, 'Al': 3,
    'K':  1, 'Ca': 2, 'Zn': 2,
    'Rb': 1, 'Sr': 2,
    'Cs': 1, 'Ba': 2,
     }

constants = {
    # [sA/mol] - https://en.wikipedia.org/wiki/Faraday_constant
    'F': 96485.332123,
    }

# -----------------------------------------------------------------------------
#                                   OXIDATION STATES
# -----------------------------------------------------------------------------
max_ox_state_metals = {
    'Sc': 3, 'Ti': 4,  'V': 5, 'Cr': 6, 'Mn': 6, 'Fe': 5,
    'Co': 5, 'Ni': 4, 'Cu': 2, 'Zn': 2, 'Ga': 3, 'Ge': 4, 'As': 5,
    'Y':  3, 'Zr': 4, 'Nb': 5, 'Mo': 6, 'Ru': 8, 'Rh': 6,
    'Pd': 4, 'Ag': 3, 'Cd': 2, 'In': 3, 'Sn': 4, 'Sb': 5,
    'La': 3, 'Ce': 4, 'Pr': 5, 'Nd': 4, 'Eu': 3, 'Gd': 3,
    'Sm': 3, 'Hf': 4, 'Ta': 5,
    'W':  6, 'Re': 7, 'Os': 6, 'Ir': 6, 'Pt': 6, 'Au': 3,
    'Tl': 3, 'Pb': 4, 'Bi': 5, 'Po': 6}

non_metal_ox_states = {
    'H':  1,
    'Li': 1, 'Be':  2, 'B':  3, 'C':  4, 'N': -3,
    'O': -2, 'F': -1, 'Na': 1, 'Mg': 2, 'Al': 3,
    'Si': 4, 'P': 5,
    'S': -2, 'Cl': -1, 'K':  1, 'Ca': 2, 'Se': -2,
    'Rb': 1, 'Sr': 2, 'Br': -1, 'Te': -2, 'I': -1,
    'Cs': 1, 'Ba': 2}
