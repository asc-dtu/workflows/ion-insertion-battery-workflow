#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 20 09:15:55 2019

Testing automatic vacancy and NEB path creation using ASE

@author: Felix Bölle <feltbo@dtu.dk>
'''
import numpy as np

# that is the one that I found to work
from ase.neighborlist import neighbor_list
from ase.utils.structure_comparator import SymmetryEquivalenceCheck


class InequivalentSiteFinder:
    '''Find inequivalent sites for ion NEB and potential calculations

    Parameters
    ----------
    atoms : ASE-atoms object
        This is the unit cell of the starting structure

    atoms_supercell : ASE-atoms object
        Supercell of structure. Based on wanted_vacancy_spacing and limited by
        max_atoms_in_cell. Created using repeat_cell() function.

    ion_str : str
        Ion intercalated or substituted.
        Symmetry analysis done on this element.

    cutoff_neb_images : float
        Cut off value for neb images in Angstromg (bird distance).

    min_vac_spacing : float
        Minimum vacancy spacing needed, otherwise will raise ValueError

    wanted_vacancy_spacing : float
        Calculated as 2*curoff_neb_images.
        This gives the spacing in between vacancies.

    vacancy_spacing : float
        In case the supercell would exceed max_atoms_in_cell, the function
        repeat_cell() reduces the vacancy spacing gradually and might not be
        the user specified value. In that case a WARNING is printed to the
        screen.

    repeat : list of ints
        How was supercell repeated, e.g. [2, 2, 2]

    single_ion_structures : list of ASE-atoms objects
        All structures with one magnesium removed from the atoms object.

    single_ion_neb_structures : list of ASE-atoms objects
        All NEB path structures with the two magnesium making the path taken
        out of the atoms_supercell object.

    all_ion_indices : list of ints
        All atom labels of the Magnesium indices in the atoms object.

    all_ion_pairs : list of tuples
        Tuples containing all start and final index of the two NEB forming ion
        pairs in the atoms object.

    unique_ion_indices : list of ints
        Only symmetrically inequivalent atom labels of the Magnesium indices in
        the atoms_supercell object.

    unique_ion_pairs : list of tuples
        Tuples containing only symmetrically inequivalent start and final index
        of the two NEB forming ion pairs in the atoms object.

    eq_to : list if ints
        This list indicates to which ion index the specific index is equivalent
        to. Together with the all_ion_indices list it is possible to identify
        which sites need to be considered. In the case of two identical ions
        with the lists all_ion_indices = [3,12] and eq_to = [3,3] it would mean
        that there only exists a single unique ion site and that ion index 12
        is symmetrically equivalent to ion index 3. A set of the two lists
        would yield [(3,3),(12,3)].
    '''

    def __init__(self, atoms=None, ion=None, cutoff_neb_images=None,
                 min_vac_spacing=8):
        self.atoms = atoms
        self.ion_str = ion
        self.cutoff_neb_images = cutoff_neb_images
        self.min_vac_spacing = min_vac_spacing

    def run(self):
        ''' Runs all necessary steps to extract symmetry inequivalent sites'''
        # ---------------------- STEP 1: CREATE SUPERCELL ----------------------
        self.create_supercell()
        # that is the dumb way, since it definitely will find equivalent sites
        # AND does all the symmetry on the supercell
        self.atoms = self.atoms_supercell
        # --------------- STEP 2: find symmetry inequivalent sites ------------
        print("\n------------------- Extracting unique Magnesium sites ------")
        self.get_unique_remove_single_ion_structures()
        self.compare_structures_ase(self.single_ion_structures,
                                    calc_type='potential')
        # ------------------------- STEP 3: FIND UNIQUE NEB PATHS -------------
        print("\n------------------- Extracting unique NEB paths ------------")
        self.get_unique_neb_paths()
        self.compare_structures_ase(self.single_ion_neb_structures,
                                    calc_type='NEB')
        print("EQV TO ", self.eq_to)
        print("ALL IND: ", self.all_ion_indices)
        print("ALL PATHS: ", self.all_ion_pairs)
        print("unique_pairs:", self.unique_ion_pairs)

    def create_supercell(self, min_vac_spacing=None):
        """ Create supercell given wanted NEB cutoff value

        Parameters
        -------------
        min_vac_spacing : int
        The minimum vacancy spacing needed, otherwise cannot run structure
        """
        if not min_vac_spacing:
            min_vac_spacing = self.min_vac_spacing

        wanted_vacancy_spacing = 2 * self.cutoff_neb_images
        atoms_to_repeat = self.atoms.copy()
        # the repeat function checks if it is possible to get the wanted
        # vacancy spacing if that is not possible, vacancy spacing will be
        # reduced!
        atoms_supercell, vacancy_spacing, repeat = self.repeat_cell(
            atoms_to_repeat,
            wanted_vacancy_spacing,
            max_atoms_in_cell=228,  # largest number in unit cell structures
            verbose=True)

        if vacancy_spacing < wanted_vacancy_spacing:
            print("WARNING: Vacancy spacing was adjusted")
            print("INITIAL WANTED SPACING: ", wanted_vacancy_spacing)
            print("ADJUSTED VACANCY SPACING: ", vacancy_spacing)
            print("ADJUSTED NEB PATH LENGTH:", vacancy_spacing/2)
            # adjust cutoff for neb_images, in case vacancy spacing changed
            # half of vacancy spacing to avoid interaction of final with
            # initial position
            self.cutoff_neb_images = vacancy_spacing/2

        if vacancy_spacing < min_vac_spacing:
            msg = ("Vacancy spacing smaller than mininmum"
                   f" {vacancy_spacing}/{min_vac_spacing}")
            raise ValueError(msg)

        self.wanted_vacancy_spacing = wanted_vacancy_spacing
        self.vacancy_spacing = vacancy_spacing
        self.atoms_supercell = atoms_supercell
        self.repeat = repeat

    def get_unique_remove_single_ion_structures(self):
        """ remove all single magnesium from the unit cell """

        atoms_list = list()
        ion_indices = [
            atom.index for atom in self.atoms if atom.symbol == self.ion_str]

        for ion_index in ion_indices:
            # always start from relaxed starting structure
            atoms_temp = self.atoms.copy()
            del atoms_temp[ion_index]
            atoms_list.append(atoms_temp)

        self.single_ion_structures = atoms_list
        self.all_ion_indices = ion_indices

    def compare_structures_ase(self, atoms_list, calc_type='potential'):
        """ Check if structures are symmetrically inqueivalent using ASE only

        Parameters
        ----------
        atoms_list : list of ASE-atoms objects

        calc_type : string
        'potential' or 'NEB' depending on which structures atoms_list contains

        """

        # get all inequivalent structures using ase
        _, agree_list_ase, eq_to_ase = self.ase_comparator(atoms_list,
                                                           angle_tol=5,
                                                           stol=0.01,
                                                           ltol=0.2,
                                                           verbose=True,
                                                           to_primitive=True)
        if calc_type == 'potential':
            ion_indices = self.all_ion_indices
        if calc_type == 'NEB':
            ion_indices = self.all_ion_pairs

        # decision making based on ases output
        unique_ion_indices = [ind for i, ind in enumerate(
            ion_indices) if agree_list_ase[i] == 'unique']

        if calc_type == 'potential':
            self.unique_ion_indices = unique_ion_indices
            self.eq_to = [self.all_ion_indices[eq]
                          for eq in eq_to_ase]  # structure index to atom index
        if calc_type == 'NEB':
            self.unique_ion_pairs = unique_ion_indices
            self.eq_to_ion_pairs = [self.all_ion_pairs[eq] for eq in eq_to_ase]

    def get_unique_neb_paths(self):
        """ Find symmetrically inequivalent paths in supercell """

        n_ion_in_structure = self.atoms.get_chemical_symbols()\
                                 .count(self.ion_str)

        # Find all ion-pairs in Neighborhood that are less than
        # self.cutoff_neb_images A away! now only look at all ion-ion neighbors
        # by provding the dictionary and wanted cutoff
        atoms_supercell = self.atoms_supercell.copy()
        first_ion_inds, second_ion_inds = neighbor_list(
            'ij',
            atoms_supercell,
            {(self.ion_str, self.ion_str): self.cutoff_neb_images})
        unique, counts = np.unique(first_ion_inds, return_counts=True)
        all_ion_pairs = [list((int(a), int(b))) for a, b
                         in zip(first_ion_inds, second_ion_inds)]
        all_ion_pairs = list(all_ion_pairs)[:sum(counts[:n_ion_in_structure])]

        # Now exclude all pairs that contain symmetrically equivalent magnesia
        # , since paths will also be symmetric
        # unique_pair[0] is the start position ion atom index in the NEB path
        all_ion_pairs = [unique_pair for unique_pair in all_ion_pairs
                         if unique_pair[0] in self.unique_ion_indices]

        # now which of those pairs is actually also a unique path !?
        # start with creating all defect NEB pair structures
        # Take out both, the start and the final magnesium element
        atoms_list = list()
        for start_ind, final_ind in all_ion_pairs:
            atoms_supercell = self.atoms_supercell.copy()
            # now delete the NEB path ion atoms
            # ASE changes atom indeces here! so old start_ind is something
            # completely different
            del atoms_supercell[[start_ind, final_ind]]
            atoms_list.append(atoms_supercell)

        self.single_ion_neb_structures = atoms_list
        self.all_ion_pairs = all_ion_pairs

    def repeat_cell(self, atoms, wanted_vacancy_spacing,
                    max_atoms_in_cell=228, verbose=True):
        """ Repeat cell to get wanted cell dimensions in each lattice direction

        Parameters
        -----------
        wanted_vacancy spacing : float
            Function will repeat cell until all cell directions will be larger
            than the specified wanted_vacancy_spacing value. Calculated as:
            wanted_vacancy_spacing = 2*cutoff_neb_images

        atoms : ASE- atoms object
            ASE-atoms object containing the starting structure

        max_atoms_in_cell : int
            Maximum atoms in cell, if that value is reached the vacancy spacing
            value will be reduced until this criterion is matched! will print a
            warning if that happens.

        verbose : boolean
            Get some information about system sizes and number of atoms

        Returns
        ---------
        atoms : ASE -  atoms object
            ASE atoms object with repeated cell and wanted dimensions

        """
        n_atoms_unit_cell = len(atoms)
        atoms_start = atoms.copy()

        # get cell lenghts
        cell = atoms.get_cell()
        cell_lengths = np.linalg.norm(cell, axis=1)
        a, b, c = cell_lengths

        x = int(((wanted_vacancy_spacing/a) + 1))
        y = int(((wanted_vacancy_spacing/b) + 1))
        z = int(((wanted_vacancy_spacing/c) + 1))

        atoms = atoms.repeat([x, y, z])

        # reduce neb_cutoff and start all over again if too many atoms in cell
        if len(atoms) > max_atoms_in_cell:
            if verbose:
                print("current vacancy spacing is:", wanted_vacancy_spacing)
            # recursive function call until requirement is reached that not more
            # than max_atoms_in_cell are in the cell
            # reduce gradually by 1% each recursive call
            atoms = self.repeat_cell(
                atoms_start,
                wanted_vacancy_spacing*0.99,
                max_atoms_in_cell=max_atoms_in_cell,
                verbose=verbose)
            return atoms

        # get cell lenghts
        cell = atoms.get_cell()
        cell_lengths = np.linalg.norm(cell, axis=1)
        a, b, c = cell_lengths

        # print some results if wanted
        if verbose:
            print("Supercell lengths after repeating unit cell:")
            print("----------------------")
            print("a: ", a)
            print("b: ", b)
            print("c: ", c)
            print("number of atoms in new cell: ", len(atoms))
            print("number of atoms in unit cell: ", n_atoms_unit_cell)
            print("Vacancy spacing in cell is at least: %.2f" % min(a, b, c))
            print("-----------------------")

        return atoms, min(a, b, c), [x, y, z]

    def ase_comparator(self, atoms_list, angle_tol=1.0, ltol=0.05, stol=0.1,
                       vol_tol=0.1, scale_volume=False, to_primitive=False,
                       verbose=False):
        """ Find symmetry equivalent structures using ASE structure comparator

        Parameters
        ----------
        atoms_list : list containing ASE-atoms object
        List containig all the ASE atoms objects that should be compared
        concerning symmetry. This can also be an e.g. ASE Trajectory.

        sim_tol :  float
        symmetry tolerance value for checking if symmetry equivalent or not

        Returns
        --------
        unique_atoms_list : list containing ASE-atoms object
        List containing all atoms objects that are symmetry inequivalent

        agree_list_ase : list of strings
        This list contains strings equal to the length of atoms_list
           - 'similar': structure is similar to any structure in atoms_list
           - 'unique': structure is unique in the atoms_list
        Note that the first structure will always be marked as 'unique' since
        it did not exist there yet

        Notes
        ------
        Read more about the tool in the source code:
        https://wiki.fysik.dtu.dk/ase/_modules/ase/utils/structure_comparator.html#SymmetryEquivalenceCheck

        """

        comp = SymmetryEquivalenceCheck(
            # angle tolerance for lattice vectors in degrees
            angle_tol=angle_tol,
            # relative tolerance for length of the lattice vectors (per atom)
            ltol=ltol,
            # position tolerance for the site comparison in units of
            # (V/N)^(1/3) (average length between atoms)
            stol=stol,
            # volume tolerance in angstrom cubed to compare the volumes of the
            # two structures
            vol_tol=vol_tol,
            # if True the volumes of the two structures are scaled to be equal
            scale_volume=scale_volume,
            # if True the structures are reduced to their primitive cells note
            # that this feature requires spglib to installed, we just input
            # the primitive cell!
            to_primitive=to_primitive)

        # now compare structures and only append if a similar one is not in
        # structure_list yet
        unique_atoms_list, agree_list_ase, equivalent_to = [], [], []
        for i_all, atoms in enumerate(atoms_list):
            if len(unique_atoms_list) == 0:
                unique_atoms_list.append(atoms)
                agree_list_ase.append('unique')
                equivalent_to.append(i_all)
            else:
                # go through all existing unique structures, only append if
                # structure is unique compared to all structures stored in
                # structure list
                for i_un, unique_atoms in enumerate(unique_atoms_list):
                    similar = comp.compare(atoms, unique_atoms)
                    if similar:
                        agree_list_ase.append('similar')
                        equivalent_to.append([i for i, s in enumerate(
                            agree_list_ase) if s == 'unique'][i_un])
                        break

                if not similar:
                    unique_atoms_list.append(atoms)
                    agree_list_ase.append('unique')
                    equivalent_to.append(i_all)

        if verbose:
            print("ASE comparator found {}/{}".format(
                   len(unique_atoms_list), len(atoms_list)) +
                  " structures to be symmetrically inequivalent")

        return unique_atoms_list, agree_list_ase, equivalent_to


if __name__ == "__main__":
    # try any atoms object out using the sample code below
    from ase.build import fcc111
    atoms = fcc111('Cu', [2, 2, 1], a=4, periodic=True)
    siteFinder = InequivalentSiteFinder(atoms=atoms,
                                        ion='Cu',
                                        cutoff_neb_images=2.9,
                                        min_vac_spacing=6)

    siteFinder.run()
