import numpy as np
from ase import Atom

from ase.neb import NEB
from iiwPBEU.neb.rneb import RNEB


class NEBPath:
    """
        This class contains functions to create a NEB path given an
        appropriate set of structures. It automatically rearranges the indices
        to be aligned in the initial and final structures. If the the final
        relaxed structure has not been calculated it will tey to create it by
        symmetry (if possible). At the moment it only supports one migrating
        atom. It should be easy to generalize this.


        Parameters
        ----------
        tol: Tolerance on atomic distances when doing symmetry analysis
        tol_path: minimum distance allowed between atoms when checking
                  the path

        """

    def __init__(self, tol=1e-3, path_tol=0.2, logname=None):
        self.tol = tol  # tolerance on atomic positions
        self.path_tol = path_tol  # tolerance on atomic positions
        self.logname = logname

    def get_neb_path(self, init, N, final=None, init0=None, final0=None,
                     orig=None, path=None, get_final=False):
        """
        Returns a NEB path with N images.
        If you already have relaxed initial and final images with well
        aligned indices you should use "create_path" instead. This method is
        intended less ideal cases.

        If the unrelaxed structures are not given it will try to create them
        from orig and the information given in path
        If the final image is not given it will attempt to create it by
        symmetry.
        If init, final, init0, and final0 is given it will only try to align
        indices between the structures.

        Parameters
        ---------

        init: ASE atoms-object
            The initial relaxed structure

        N: integer
            Number of images

        final: ASE atoms-object
            The final relaxed structure

        init0: ASE atoms-object
            The initial unrelaxed structure

        final0: ASE atoms-object
            The final unrelaxed structure

        orig: ASE atoms-object
            The original pristine supercell without any "defects"

        path: list of lists
            information about the defects given as
            path = [[pos_i, pos_f, symb_i, symb_f, dtype],
                    [pos_i, pos_f, symb_i, symb_f, dtype]]

            pos_i: initial position of the defect
            pos_f: final position of the defect
            symb_i: initial chemical symbol of the defect
            symb_f: final chemical symbol of the defect
            dtype: the defect type (vacancy, interstitial, or displacement)

        Return
        -------

        A NEB path with N images: list of ASE-atoms objects
        """
        if init0 is None and final0 is None:
            init0, final0 = self.get_unrelaxed_structures(orig, path)
        if final is None:
            rneb = RNEB(tol=self.tol, logname=self.logname)
            final0, final = self.align_indices(init0, final0)
            final = rneb.get_final_image(orig, init0, init, final0)
        else:
            final0, final = self.align_indices(init0, final0, final=final)
        images = self.create_path(init, final, N)
        sane_path = self.check_path(images, tol=self.path_tol)
        if not sane_path:
            print("WARNING, atoms pass within {} Angstrom of each other"
                  .format(self.path_tol))
        if get_final:
            return final, final0
        return images

    @staticmethod
    def check_path(images, tol=0.1):
        """
        Check in all images that no atoms are closer than tol to idx.
        return False of atoms are to close else True.

        Parameters
        ---------

        images: list of ASE atoms-objects
        The NEB path.

        tol: Float
        Tolerance for how close atoms are allowed to be (Angstrom).
        """

        # find the moving atom
        pos_init = images[0].get_positions()
        pos_final = images[-1].get_positions()
        dists = np.linalg.norm(pos_init - pos_final, axis=1)
        idx = np.argmax(dists)
        # check if atoms are too close
        for im in images:
            ind_neighbors = [a.index for a in im if a.index != idx]
            dist = im.get_distances(idx, ind_neighbors, mic=True)
            if any(dist < tol):
                return False
        return True

    def create_path(self, init, final, N):
        """
        A wrapper function get an NEB path by interpolating between the
        initial and final images.

        Parameters
        ---------

        init: ASE atoms-object
        The initial structure

        final: ASE atoms-object
        The final structure

        N: integer
        Number of images

        Return
        -------

        A NEB path with N images: list of ASE-atoms objects


        """

        m = N - 2  # number of intermediate images
        images = [init]
        images += [init.copy() for i in range(m)]
        images += [final]
        neb = NEB(images)
        neb.interpolate(mic=True)
        return images

    def align_indices(self, init0, final0, final=None):
        """
        move the atoms in final0 (and optionally in final) such that the
        indices match with init0.

        Parameters
        ---------

        init0: ASE atoms-object
        The initial unrelaxed structure

        final0: ASE atoms-object
        The final unrelaxed structure

        final: ASE atoms-object
        The final relaxed structure

        Return
        -------

        two resorted ASE-atoms objects: [ASE atom-object,ASE atom-object]
        """

        sort_list = self.match_atoms(init0, final0)
        final0_sorted = final0.copy()
        final0_sorted = final0_sorted[sort_list]
        if final is not None:
            final = final[sort_list]
        return final0_sorted, final

    def match_atoms(self, init, final):
        """
        Match the atomic indices in final to the ones in init based on
        position

        Parameters
        ---------

        init: ASE atoms-object
        The initial unrelaxed structure

        final: ASE atoms-object
        The final unrelaxed structure

        Returns
        -------

        sorting list: numpy array 1 x n of integers
        """
        pos_init = init.get_positions()
        pos_final = final.get_positions()
        symb_init = init.get_chemical_symbols()
        symb_final = final.get_chemical_symbols()
        sort_list = np.ones(len(init)).astype(int)*-1
        no_match = []
        for idx_final, state in enumerate(zip(pos_final, symb_final)):
            pf, sf = state
            idx_init = self.find_atom(pos_init, symb_init, pf, sf,
                                      critical=False)
            if idx_init is False:
                no_match.append(idx_final)
            else:
                sort_list[idx_init] = idx_final
        missing = np.where(sort_list == -1)
        if len(missing) != len(no_match):
            if missing[0] != no_match[0]:
                msg = ("different number of atoms have moved "
                       "in the initial and final structures")
                raise RuntimeError(msg)

        if len(no_match) > 1:
            msg = ("Found more than one moving atom. "
                   "Please give information about where "
                   "the moving atoms start and finish")
            raise RuntimeError(msg)
        sort_list[missing[0]] = no_match[0]
        return sort_list

    def find_atom(self, pos, symb, pos_d, symb_d, critical=True):
        """
        Find the atom matching pos_d.

        Parameters
        ---------

        pos: numpy array n x 3
        positions to be matched against

        sym: string
        chemical symbols to be matched against

        pos_d: numpy array 1 x 3
        position to be found

        sym_d: string
        chemical symbols to be found

        critical: boolean
        whether to fail upon not being able to make a match

        Return
        -------

        integer if match is found otherwise False: integer or boolean
        """
        for i, state in enumerate(zip(pos, symb)):
            p, s = state
            if s == symb_d:
                dist = np.linalg.norm(p-pos_d)
                if dist < self.tol:
                    return i
        if critical:
            msg = ("The speified atoms was not found. "
                   "Make sure you given the correct positions and symbols")
            raise RuntimeError(msg)
        else:
            return False

    def get_unrelaxed_structures(self, orig, path):
        """
        Create unrelaxed structures by introducing defects into the
        pristine supercell

        Parameters
        ---------

        orig: ASE-atoms object
        The original supercell without defects

        path: list of lists
        see documentation for get_neb_path

        Return
        --------
        two ASE-atoms objects: [ASE-atoms object, ASE-atoms object]
        """
        init0 = orig.copy()
        final0 = orig.copy()
        for i, p in enumerate(path):
            dtype = p[4]
            if dtype == 'vacancy':
                init_idx, final_idx = self.look_for_atoms(init0, final0, p)
                del init0[init_idx]
                del final0[final_idx]
            if dtype == 'interstitial':
                init0, final0 = self.insert_atoms(orig, p)
            if dtype == 'displacement':
                init_idx, final_idx = self.look_for_atoms(init0, final0, p,
                                                          find_final=False)
                pos_f = p[1]
                final0[init_idx].position = pos_f

        return init0, final0

    def insert_atoms(self, orig, p):
        """
        Insert atoms in orig based on the positions
        and symbols of p.

        Parameters
        ---------

        orig: ASE-atoms object
            The original supercell without defects

        p: list
            A single list from path. See documentation for get_neb_path

        Return
        --------
        The unrelaxed initial and final structures:
        [ASE-atoms object, ASE-atoms object]
        """
        pos_i = p[0]
        pos_f = p[1]
        symb_i = p[2]
        symb_f = p[3]
        init0 = orig.copy()
        final0 = orig.copy()
        init = init0 + Atom(symb_i, position=pos_i)
        final = final0 + Atom(symb_f, position=pos_f)
        return init, final

    def look_for_atoms(self, init0, final0, p, find_final=True):
        """
        identifies the indices of the atoms belonging to the positions
        specified in p.

        Parameters
        ---------

        init0: ASE atoms-object
            The initial unrelaxed structure

        final0: ASE atoms-object
            The final unrelaxed structure

        p: list
            A single list from path. See documentation for get_neb_path

        Return
        --------
        Indices of the atoms specified by p: [integer, integer]
        """
        pos_i = p[0]
        pos_f = p[1]
        symb_i = p[2]
        symb_f = p[3]
        pos_init = init0.get_positions()
        symb_init = init0.get_chemical_symbols()
        pos_final = final0.get_positions()
        symb_final = final0.get_chemical_symbols()
        init_idx = self.find_atom(pos_init, symb_init, pos_i, symb_i)
        if find_final:
            final_idx = self.find_atom(pos_final, symb_final, pos_f, symb_f)
        else:
            final_idx = False
        return init_idx, final_idx
