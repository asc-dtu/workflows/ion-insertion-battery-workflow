"""
Created on Tue Mar 17 13:38:58 2020

Base Task for all bulk relaxation tasks

@author: felix
"""


class TaskRelax():
    """ Base Task for all bulk relaxations """

    def retrieve_information():
        """ get needed information for executing calculation """

    def get_atoms_object():
        """ Return an ASE atoms object """

    def get_calculator_object():
        """ Return an ase calculator object """

    def relax():
        """ Relax atoms """
