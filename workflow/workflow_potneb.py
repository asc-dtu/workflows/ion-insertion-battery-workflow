# =============================================================
#      WORKFLOW SCRIPT FOR Mg-Potential/NEB calculations
# =============================================================

import os
import json

from myqueue.task import task
from iiwPBEU.battery_tools import get_supercell_from_folder

def get_resources(atoms, ion_out, ion_max, n_cores_max=4, cpus_per_core=40):
    """ returns needed computational resources for given cell size """
    resources = '80:xeon40:1:2d'
    return resources


# retrieve information from json dictionary
with open('potneb_dictionary.json') as f:
    dic = json.load(f)
with open('current_material.json') as f:
    user_args = json.load(f)
# retrive folder paths
pot_folders = dic['pot_folders']
pot_folders_with_U = dic['pot_folders_with_U']
neb_folders = dic['neb_folders']

# filter out neb tasks if not all are wanted here!
# E.g. only relfective ones needed?

# supercell for resource selection
supercell = get_supercell_from_folder(0)

def create_tasks():
    tasks = []

    tasks_potential = []
    for pot_folder in pot_folders_with_U:
        ion_out,ion_max,ion_ind = pot_folder.split('/')[-3].split('_')
        resources = get_resources(supercell,ion_out,ion_max)
        tasks_potential.append(task('iiwPBEU.relax_potential_vasp', 
                                    resources=resources,
                                    folder=pot_folder))

    t_check = [task('iiwPBEU.check_potential', 
                    resources='24:xeon24:1:10m',
                    folder=os.getcwd(),
                    deps=tasks_potential)]

    tasks_pot_no_U = []
    for pot_folder in pot_folders:
        ion_out,ion_max,ion_ind = pot_folder.split('/')[-3].split('_')
        resources = get_resources(supercell, ion_out, ion_max)
        tasks_pot_no_U.append(task('iiwPBEU.relax_potential_vasp',
                                   resources=resources,
                                   folder=pot_folder,
                                   deps=t_check))

    tasks_nebs = []
    tasks_cineb = []
    for folders in neb_folders:
        # reflective
        if folders[0].split('/')[-3] == 'RMINEB':
            neb_folder, cirneb_folder = folders
            init_ind, final_ind ,ion_out, ion_max = neb_folder.split('/')[-4].split('_')
            resources = get_resources(supercell,ion_out,ion_max)
            neb_task = task('iiwPBEU.relax_neb_vasp',
                            resources=resources,
                            folder=neb_folder,
                            deps=tasks_pot_no_U)
            tasks_nebs.append(neb_task)
            tasks_cineb.append(task('iiwPBEU.relax_cirneb_vasp',
                                    resources=resources,
                                    folder=cirneb_folder,
                                    deps=[neb_task]))
        # non reflective
        elif folders[0].split('/')[-3] == 'CNEB':
            neb_folder, cicneb_folder = folders
            init_ind, final_ind ,ion_out, ion_max = neb_folder.split('/')[-4].split('_')
            resources = get_resources(supercell,ion_out,ion_max)
            neb_task = task('iiwPBEU.relax_neb_vasp',
                            resources=resources,
                            folder=neb_folder,
                            deps=tasks_pot_no_U)
            tasks_nebs.append(neb_task)
            tasks_cineb.append(task('iiwPBEU.relax_cicneb_vasp',
                                    resources=resources,
                                    folder=cicneb_folder,
                                    deps=[neb_task]))
   
    tasks = tasks_potential + t_check + tasks_pot_no_U + tasks_nebs + tasks_cineb

    return tasks
