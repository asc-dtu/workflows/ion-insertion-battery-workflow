"""
Script for collecting data for ion battery workflow
"""

from contextlib import contextmanager
import os
import json
import glob
import json
import numpy as np
from pathlib import Path

from ase.calculators.singlepoint import SinglePointCalculator
from ase.db import connect
from ase.db.row import atoms2dict
from ase.io import read, Trajectory
from ase.neb import NEBTools

from iiwPBEU.convex_hull import get_ch_hof_energy
import iiwPBEU.battery_tools as bt
from iiwPBEU.data import n_val_ele


@contextmanager
def cd(newdir):
    """ 
    change folder using contextmanager without creating directories
    """
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)
      
# ------------- COLLECT FUNCITONS ---------------------------------

class CollectData:
    """ Helper class to collect data for the ion insertion battery workflow """

    def __init__(self, 
                 ion, 
                 ms,
                 functional,
                 db,
                 uc_full_p,
                 uc_empty_p,
                 uc_empty_scaled_p,
                 db_ref):
        self.ion = ion
        self.ms = ms
        self.functional = functional
        self.db = db
        self.uc_full_p = uc_full_p
        self.uc_empty_p = uc_empty_p
        self.uc_empty_scaled_p = uc_empty_scaled_p
        self.db_ref = db_ref



    def check_done(self, done_f, path = './'):
        if os.path.isfile(path + done_f):
            return True
        return False
    
    def collect_potneb_dic_info(self, atoms, material_id, db):
        try:
            with open('potneb_dictionary.json','r') as f:
                dic = json.load(f)
        except:
            return
        refl_info = dic["is_reflective"]
        for info in refl_info:
            ((neb_path,remove_n_ions),refl) = info
            if refl: refl = True;
            else: refl = False;
            db.write(atoms,
                     material_id=material_id,
                     neb_path=f"_{neb_path[0]}_{neb_path[1]}",
                     rem_n=remove_n_ions,
                     reflective=refl)
    
    def collect_dictionaries_home(self, data):
        with open('potneb_dictionary.json','r') as f:
            potneb_dic = json.load(f)
        with open('current_material.json','r') as f:
            user_input_dic = json.load(f)
            
        data['potneb_dic'] = potneb_dic
        data['user_input'] = user_input_dic
        return data
    
    def collect_supercell(self, data):
        supercell = bt.get_supercell_from_folder(0)
        N_sc = supercell.get_chemical_symbols().count(self.ion)
        row_id = db.write(supercell)
        
        data['supercell_final'] = supercell.todict()
        
        self.supercell = supercell
        self.row_id = row_id
        db.update(id=self.row_id, 
                  ion=self.ion,
                  material_id=self.material_id,
                  magstate=self.ms,
                  functional=self.functional)
        
        self.N_sc = N_sc
        return data
    
    def print_magmoms(self, atoms, name):
        if self.ms != 'NM':
            print(f"{name} :","{%5.2f}"%(atoms.get_magnetic_moment()), " [mu]")

    def collect_volume_changes(self, data, print_magmoms=False):
        
        with open('current_material.json') as f:
            user_args = json.load(f)  
        remove_n_ions = user_args['remove_n_ions']
        
        if remove_n_ions == 'single_one':
            
            uc_full = read(self.uc_full_p + 'OUTCAR')
            
            if print_magmoms:
                self.print_magmoms(uc_full,"bulk")
                
            v_full = uc_full.get_volume()
            
            self.uc_full = uc_full
            
            init, final = self.get_init_final_atoms(self.uc_full_p)
            data['optimized_discharged_unit_cell_final'] = final.todict()
            data['optimized_discharged_unit_cell_init'] = init.todict()
        
        else:
            uc_full = read(self.uc_full_p + 'OUTCAR')
            uc_empty = read(self.uc_empty_p + 'OUTCAR')
            uc_empty_scaled = read(self.uc_empty_scaled_p + 'OUTCAR')
            
            if print_magmoms:
                self.print_magmoms(uc_full,"bulk")
                self.print_magmoms(uc_empty,"empty")
                self.print_magmoms(uc_empty_scaled,"empty scaled")
            
            v_full = uc_full.get_volume()
            v_empty = uc_empty.get_volume()
            v_change = ((v_empty - v_full)/v_full) * 100
            
            self.db.update(id = self.row_id, v_change = v_change)
            uc_repeated = int(len(self.supercell)/len(uc_full))
            
            self.uc_repeated = uc_repeated
            self.uc_full = uc_full
            self.uc_empty = uc_empty
            self.uc_empty_scaled = uc_empty_scaled
            
            # store structures in data
            init, final = self.get_init_final_atoms(self.uc_full_p)
            data['optimized_discharged_unit_cell_final'] = final.todict()
            data['optimized_discharged_unit_cell_init'] = init.todict()
            
            init, final = self.get_init_final_atoms(self.uc_empty_p)
            data['optimized_charged_unit_cell_init'] = init.todict()
            data['optimized_charged_unit_cell_final'] = final.todict()
            
            init, final = self.get_init_final_atoms(self.uc_empty_scaled_p)
            data['optimized_charged_constraint_unit_cell_init'] = init.todict()
            data['optimized_charged_constraint_unit_cell_final'] = final.todict()
               
        return data
  
    def get_init_final_atoms(self, path):
        init = read(path + 'OUTCAR_0', index=0) # first configuration
        final = read(path + 'OUTCAR') # last configuration
        return init, final

    # collect_ch_energies for single one case
    def collect_ch_energies(self, db_ref):
        
        with open('current_material.json') as f:
            user_args = json.load(f)  
        remove_n_ions = user_args['remove_n_ions']
        
        if remove_n_ions == 'single_one':
            try:
                self.db.get(id=self.row_id).e_ch_full
                return
            except AttributeError:
                pass
        
            total_e_full = self.uc_full.get_potential_energy()
            _, e_ch_full = get_ch_hof_energy(self.uc_full, total_e_full, db_ref)

            self.db.update(id=self.row_id, e_ch_full=e_ch_full)
        
        else:
            # already collected?
            try:
                self.db.get(id=self.row_id).e_ch_full
                self.db.get(id=self.row_id).e_ch_empty
                return
            except AttributeError:
                pass
            
            total_e_full = self.uc_full.get_potential_energy()
            _, e_ch_full = get_ch_hof_energy(self.uc_full, total_e_full, db_ref)

            total_e_empty = self.uc_empty.get_potential_energy()
            _, e_ch_empty = get_ch_hof_energy(self.uc_empty, total_e_empty, db_ref)
            
            self.db.update(id=self.row_id, e_ch_full=e_ch_full, e_ch_empty=e_ch_empty)

    def get_potential(self, discharged_e, charged_e, N_ion, ion):
        """ calculates potential
        
        Parameters
        ----------
        N_ion : integer
        how many ions to balance the equation, difference between 
        charged/discharged state
        
        """
        row = self.db_ref.get(elements=ion)
        ion_energy = row.energy/len(row.toatoms())
        return (discharged_e - (charged_e + N_ion*ion_energy))               / (n_val_ele[ion]*N_ion) # since 2 electrons per magnesium
    
    
    def collect_avg_pot(self):
        # already collected?
        try:
            self.db.get(id=self.row_id).pot_avg
            return
        except AttributeError:
            pass
        
        bulk_energy = self.uc_full.get_potential_energy()
        pot_energy = self.uc_empty.get_potential_energy()
    
        N_uc = self.uc_full.get_chemical_symbols().count(self.ion)
    
        pot_0 = self.get_potential(bulk_energy, pot_energy, N_ion = N_uc,
                              ion = self.ion)
        self.db.update(id = self.row_id, pot_avg=pot_0)
    
    def collect_conc_pot(self, 
                         pot_atoms,
                         n_ion,
                         N_ion,
                         ind,
                         pot_low,
                         pot_high,
                         index,
                         print_magmoms=True):
        n_ion = int(n_ion)
        N_ion = int(N_ion)
        
        # already collected?
        try:
            if n_ion + 1 == N_ion:
                attribute = f'pot_low_{ind}'
            else:
                attribute = f'pot_high_{ind}'
            vars(self.db.get(id=self.row_id))[attribute]
            return vars(self.db.get(id=self.row_id))[attribute]
        except KeyError:
            pass
        
        pot_energy = pot_atoms.get_potential_energy()
        
        if print_magmoms:
            self.print_magmoms(
                pot_atoms,f"potential n_ion {n_ion} index {index}:")

        conc = 100 - (n_ion/N_ion) * 100
        if n_ion + 1 == N_ion:
            # low concentration case
            bulk_energy = self.uc_empty_scaled.get_potential_energy()*self.uc_repeated
            pot = self.get_potential(pot_energy, bulk_energy, N_ion = 1,
                                 ion = self.ion)
            self.db.update(id = self.row_id, conc_low = conc)
            if pot < pot_low:
                pot_low = pot
                self.db.update(id = self.row_id, pot_low=pot_low)
        else:
            # high concentration case
            bulk_energy = self.uc_full.get_potential_energy()*self.uc_repeated
            pot = self.get_potential(bulk_energy, pot_energy, N_ion = n_ion,
                                 ion = self.ion)
            self.db.update(id=self.row_id, conc_high=conc)
            if pot < pot_high:
                pot_high = pot
                self.db.update(id=self.row_id, pot_high=pot_high)

        return pot


    def collect_conc_neb(self,
                         path,
                         n_ion,
                         N_ion,
                         unique_ind,
                         ind_2,
                         ind,
                         neb_type,
                         data,
                         print_magmoms=False):
        n_ion = int(n_ion)
        N_ion = int(N_ion)

        # how many images were used? Hard coded for now
        N_img_dic = {'CIRNEB': 5,
                     'RMINEB': 3,
                     'CINEB': 9}
        N = N_img_dic[neb_type]
        # read in relaxed path
        neb_traj = read(path + f'neb.traj@-{N}:')
    
        if print_magmoms:
            self.print_magmoms(
                neb_traj[1],f"NEB n_ion {n_ion} ind {unique_ind}-{ind_2}")
        #db.write(neb_traj[1])
 
        # no U correciton for inital structure
        path_init ='./potential/{}_{}_{}/{}/vasp_rx_{}/'.format(n_ion,N_ion,
                                                                unique_ind,
                                                                self.ms,
                                                                self.functional)
        init = read(path_init + 'OUTCAR')
        results = init.calc.results
        init_e = results["energy"]
        init_f = results["forces"]
        try:
            init_m = results["magmom"]
        except KeyError:
            init_m = np.array([0 for atom in init])
        # get correct indexing in case VASP did something here
        restart_f = 'ase-sort_0.dat'
        if Path(path_init + restart_f).is_file():
            sort, sort_ase  = bt.get_vasp_restart_sort_list(path_init)
            init = init[sort_ase]
        else:
            sort,sort_ase = bt.get_vasp_restart_sort_list(path_init, sortfile = 'ase-sort.dat')
            init = init[sort_ase]

        # now map the forces on the final image since that has no calculator yet
        # this is only needed when one want to use the NEBtool ase interpolation

        # assign values to first image
        newcalc = SinglePointCalculator(neb_traj[0], **results)
        neb_traj[0].calc = newcalc

        bar_found_rmi = False
        if neb_type == 'RMINEB':
            dic = self.read_reflective(path) # symmetry dic for force reflection
            neb_traj = self.reflect_results_on_final(
                neb_traj,init_e, init_f, init_m, dic)
        elif neb_type == 'CINEB':
            # this is the wanted behavior
            init_path = read(path + 'initial_path.traj' + '@:')
            init = init_path[0]
            newcalc = SinglePointCalculator(neb_traj[0], **init.calc.results)
            neb_traj[0].set_calculator(newcalc)

            final = init_path[-1]
            newcalc = SinglePointCalculator(neb_traj[-1], **final.calc.results)
            neb_traj[-1].set_calculator(newcalc)
        elif neb_type == 'CIRNEB':
            bar_found_rmi = self.bar_found_rmi_neb(path)
            db.update(id=self.row_id)
        else:
            # for now pass
            pass

        # store specific barrier of path
        if bar_found_rmi:
            # assign the RMI-NEB barrier
            rmi_key = f'RMINEB_bar_{n_ion}_{N_ion}_{unique_ind}_{ind_2}'
            bar = vars(self.db.get(id=self.row_id))[rmi_key]
            # make a count on how often that happened
            self.add_bar_found_counter()
        else:
            # extract the data using fit or not, compare those two
            #nebtools = NEBTools(neb_traj)
            #barrier_fit = nebtools.get_barrier(fit = True)[0]
            #barrier_no_fit = nebtools.get_barrier(fit = False)[0]
            # can this function be deleted? I dont understand what it is doing
            bar_no_ase = self.get_min_bar(neb_traj)
            # extract barrier
            bar = self.get_bar_no_fit(neb_traj)
            
        kwargs = {f'{neb_type}_bar_{n_ion}_{N_ion}_{unique_ind}_{ind_2}': bar}
        

        self.db.update(id=self.row_id, **kwargs)

        # store initial and final path as well        
        init, final = self.get_init_final_images(
            path, N, ind_folder=f'{n_ion}_{N_ion}_{unique_ind}_{ind_2}')

        data = self.add_images_to_data(data, init,
                name = f'neb_path_{neb_type}_{n_ion}_{N_ion}_{unique_ind}_{ind_2}_init')
        data = self.add_images_to_data(data, final,
                name = f'neb_path_{neb_type}_{n_ion}_{N_ion}_{unique_ind}_{ind_2}_final')
        
        # store energies of images separately since not copied when atoms.todict()
        if not bar_found_rmi:
            es_images = [atoms.get_potential_energy() for atoms in neb_traj]
            es_key = f'{neb_type}_es_{n_ion}_{N_ion}_{unique_ind}_{ind_2}'
            es_dic = {es_key: es_images}
            data.update(es_dic)
        
        return data
    
    def add_bar_found_counter(self, bar_found_rmi_key='bar_found_rmi'):
        """ Count how often RMI-NEB barrier is also CIRNEB barrier in mat """
        n_bar_found = 0
        try:
            n_bar_found = vars(self.db.get(id=self.row_id))[bar_found_rmi_key]
        except KeyError:
            pass
        n_bar_found += 1
        self.db.update(id=self.row_id, **{bar_found_rmi_key: n_bar_found})
        
        
    def bar_found_rmi_neb(self, path):
        # always take latest outfile
        files = [(os.path.getmtime(os.path.join(path, fs)),fs) for fs in os.listdir(path) 
                 if fs.endswith('.out')]
        out_file = sorted(files)[-1][1]
        # this is written by the workflow, could also be tested again with
        # taking the energies
        out_line_1 = "Transition state has already been found using RMINEB"
        out_line_2 = "Transition state already found using RMINEB"
        with open(os.path.join(path, out_file), 'r') as f:
            for line in f:
                if line.rfind(out_line_1) > -1:
                    return True
                if line.rfind(out_line_2) > -1:
                    return True
        return False

    def add_images_to_data(self, data, images, name):
        image_dic = {name: {}}
        for i, atoms in enumerate(images):
            image_dic[name].update({f'image_{i}': atoms.todict()})
        data.update(image_dic)
        return data

    def get_init_final_images(self, path, N, ind_folder):
        init = Trajectory(path + 'initial_path.traj')
        final = Trajectory(path + 'neb.traj')[-N:]
        return init, final

    def get_min_bar(self, images):
        Es = np.array([atoms.get_potential_energy() for atoms in images])
        Es_norm = Es - Es[0]
        return min(Es)

    def get_bar_no_fit(self,images):
        """ Returns maximum height of barrier """
        Es = np.array([atoms.get_potential_energy() for atoms in images])
        # normalize to minimum
        Es_norm = Es - min(Es)
        return max(Es_norm)

    def calc_diffusion_coefficient(barrier, l):
        """ Calculate diffusion coefficient according to Arrhenius approach
        
        Parameters
        -----------
        barrier : float
        diffusion barrier in eV
        
        l : float
        Diffusion path length in Angstrom
        """
        l = l*1-8 # [cm]
        v = 1e+13 # [1/s] attempt frequency
        kb = 8.617*1e-5 # [eV/K] Boltzmann constant from wikipedia
        T = 300 # [K]

        D = (l**2) * v * np.exp(-(barrier/(kb*T))) # [cm^2/s]

        return D


    def read_reflective(self, path):
        with open(path + 'symmetry.json', 'r') as f:
            dic = json.load(f)

        return dic

    def reflect_results_on_final(self,traj, init_e, init_f, init_m, dic):
        sym = dic["symmetries"][0]
        forces_temp = np.array([np.dot(sym[0],init_f[at]) for at in sym[2]]) # rotate the forces

        results = {}
        results['energy'] = init_e
        results['forces'] = forces_temp
        results['magmom'] = init_m

        # assign to image
        newcalc = SinglePointCalculator(traj[-1], **results)
        traj[-1].set_calculator(newcalc)
        return traj

    def collect_potentials(self, data):
        pot_folders = os.listdir('./potential')
        pot_high = 10
        pot_low = 10
        for ind,pot_folder in enumerate(pot_folders):
            path = f'./potential/{pot_folder}/{self.ms}/vasp_rx_{self.functional}_U/'
            path_no_U = f'./potential/{pot_folder}/{self.ms}/vasp_rx_{self.functional}/'
            if not collect.check_done('iiwPBEU.relax_potential_vasp.done', path):
                continue
            n_ion, N_ion, index = pot_folder.split('_')
            pot_atoms = read(path + 'OUTCAR')
            #db.write(pot_atoms)
            pot_temp = collect.collect_conc_pot(
                pot_atoms, n_ion, N_ion, ind, pot_low, pot_high, index)

            n_ion = int(n_ion)
            N_ion = int(N_ion)
            if n_ion + 1 == N_ion:
                pot_low = pot_temp
            else:
                pot_high = pot_temp
                
            self.db.update(id=self.row_id, 
                           **{f'pot_{n_ion}_{N_ion}_{index}': pot_temp})
            
            data = self.get_pot_structures(
                data, path, path_no_U, pot_folder, n_ion)
        
        return data
    
    def get_pot_structures(self, data, path, path_no_U, pot_folder, n_ion):
        init, final = self.get_init_final_atoms(path)
        if n_ion == 1:
            data[f'supercell_low_soc_{pot_folder}_init'] = init.todict()
            data[f'supercell_low_soc_{pot_folder}_final'] = final.todict()    
        else:
            data[f'supercell_high_soc_{pot_folder}_init'] = init.todict()
            data[f'supercell_high_soc_{pot_folder}_final'] = final.todict()
            
        if not collect.check_done('iiwPBEU.relax_potential_vasp.done', path_no_U):
            return data
        
        init, final = self.get_init_final_atoms(path_no_U)
        if n_ion == 1:
            data[f'supercell_low_soc_{pot_folder}_no_U_init'] = init.todict()
            data[f'supercell_low_soc_{pot_folder}_no_U_final'] = final.todict()
        else:
            data[f'supercell_high_soc_{pot_folder}_no_U_init'] = init.todict()
            data[f'supercell_high_soc_{pot_folder}_no_U_final'] = final.todict()
            
        return data

    def collect_nebs(self,
                     data,
                     neb_types=['RMINEB', 'CIRNEB', 'CNEB', 'CICNEB']):
        neb_folders = os.listdir('./NEBs')
        
        task_done_f = {'CIRNEB': 'iiwPBEU.relax_cirneb_vasp.done',
                       'RMINEB': 'iiwPBEU.relax_neb_vasp.done',
                       'CNEB':  'iiwPBEU.relax_neb_vasp.done',
                       'CICNEB':  'iiwPBEU.relax_cicneb_vasp.done'}

        for ind,neb_folder in enumerate(neb_folders):
            for neb_type in neb_types:
                path = f'./NEBs/{neb_folder}/{neb_type}/{self.ms}/vasp_rx_{self.functional}/'
                if not collect.check_done(task_done_f[neb_type], path):
                    continue
                
                unique_ind,ind_2, n_ion, N_ion = neb_folder.split('_')
                print("neb folder now:", neb_folder)
                data = collect.collect_conc_neb(
                    path, n_ion, N_ion, unique_ind,ind_2,ind, neb_type, data)

        return data


class DBPostProcessor:
    """ Post process data just collected without accessing folders """
    def __init__(self, db, row_id):
        self.db = db  # database to access
        self.row_id = row_id
        self.neb_types = ['RMINEB', 'CIRNEB', 'CNEB', 'CICNEB']
        
    def extract_minimum_neb_path(self):
        """ Takes in all paths and finds connected ones ultimately """
        row = self.db.get(id=self.row_id)
        # differentiate between concentrations
        # get all keys containing a neb path and barrier value
        neb_bar_keys = []
        for key in vars(row).keys():
            if any([key.startswith(neb_type) for neb_type in self.neb_types]):
                neb_bar_keys.append(key)
        
        # sort keys according to type and concentration
        neb_low_keys = [] # low SOC equals n_ion == 1
        neb_high_keys = []
        for key in neb_bar_keys:
            pass

# -------------- INPUT SPECIFICATIONS -----------------------------------------
# connect/create to database that stores information
db = connect('iiw_data.db')

# which functional to be collected
functional = 'PBESOL'

# which reference database for calculation of ch
db_ref = bt.connect_to_ref_db(0)
# -----------------------------------------------------------------------------

with cd('calculations'):
    all_ions = os.listdir()
for ion in all_ions:
    with cd('calculations'+'/'+ion):
        all_materials = os.listdir()
    for material in all_materials:
        with cd('calculations'+'/'+ion+'/'+material):
            with open('./current_material.json') as f:
                user_args = json.load(f)
                
            remove_n_ions = user_args['remove_n_ions']
            ms = user_args['magstate']
            bulk_path = f'./bulk/{ms}/vasp_rx_{functional}_U/'
            
            if remove_n_ions == 'single_one':
                bulk_empty_path = None
                bulk_empty_scaled_path = None
            else:
                bulk_empty_path = f'./bulk_empty/{ms}/vasp_rx_{functional}_U/'
                bulk_empty_scaled_path = f'./bulk_empty_scaled/{ms}/vasp_rx_{functional}_U/'
                
            collect = CollectData(ion=ion,
                      ms=ms,
                      functional=functional,
                      db=db,
                      uc_full_p=bulk_path,
                      uc_empty_p=bulk_empty_path,
                      uc_empty_scaled_p=bulk_empty_scaled_path,
                      db_ref=db_ref)

            collect.material_id = material
            print("current material ID:", material)
            data = {} # store all no sql things in here
            
            if remove_n_ions == 'single_one':
                pass
            else:
                if not os.path.exists(bulk_empty_scaled_path + 'OUTCAR'):
                    continue
                    
            # Supercell first, otherwise not bother to collect anything
            if not collect.check_done('iiwPBEU.prepare_potneb_vasp.done'):
                continue
                
            data = collect.collect_supercell(data)
        
            # dictionaries for later use
            data = collect.collect_dictionaries_home(data)

            # collect volume changes
            # collect empty potential
            # collect potential
            if remove_n_ions == 'single_one':
                data = collect.collect_volume_changes(data)
            else:
                if not collect.check_done('iiwPBEU.relax_empty_scaled_vasp.done',
                                       bulk_empty_scaled_path):
                    continue
                data = collect.collect_volume_changes(data)
                collect.collect_avg_pot()
                data = collect.collect_potentials(data)


            # collect ch_energies
            collect.collect_ch_energies(db_ref)
 
            # collect NEB calculations and diffusivities
            data = collect.collect_nebs(data)
            
            # save all data in db
            db.update(id=collect.row_id, data=data)
