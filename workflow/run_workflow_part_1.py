# ===================================================================
#         RUN THE FIRST PART OF THE WORKFLOW (BULK+EMPTY)
# ===================================================================

import subprocess
import json
from ase.db import connect
from iiwPBEU.pre_screen import count_metal_oxidation_state,\
                               get_specific_neighbors_voronoi,\
                               check_atom_percolating

db = connect('./starting_structures/materials.db')
material_ids_reflective = ['mp-676282',  # Chevrel
                           'mp-27872',  # MgTi2O4
                           ]

# workflow input
magstate = 'FM'  # magnetic state
ion = 'Mg'  # which ion will be substituted

# longest considered NEB path, bird length
# Note that the vacancy spacing is calculated as 2*cut_off_neb !
# The cut_off_neb value does not change, but the vacancy spacing
# might be adjusted if the supercell would grow too large
cut_off_neb = 5.5

# thresholds
potential = -200  # in (eV)
diffusivity = 2  # in (eV)
ch_energy = 0.5  # in (eV)
volume_change = 100  # in (%)

material_ids = material_ids_reflective

for material_id in material_ids:
    # first some user defined checks, is metal oxidation state already max?
    atoms = db.get_atoms(material_id=material_id)
    formula = db.get(material_id=material_id).formula
    _, _, frac_ion_removable = count_metal_oxidation_state(formula, ion = ion)
    if frac_ion_removable == 0:
        print(f"Max oxidation state for metal in {formula}, wont run this since ion removable {frac_ion_removable}")
        continue

    # next look at ion coordination
    anions = ['O', 'S']  # elements coordinating ion
    preferred_coord_ion = 6  # Mg preferred coordination
    n_neighbors = get_specific_neighbors_voronoi(atoms,
                                                 element=ion,
                                                 neighbor_els=anions)
    print("Coordination numbers for {}: {}".format(formula, n_neighbors))
    if all(n_neighbors == preferred_coord_ion):
        print("Preferred coordination of ion in {formula} found, diffusion barrier will be high, not running this")
        continue

    # check percolating path
    inds = [atom.index for atom in atoms if atom.symbol == ion]
    for ind in inds:
        percolating = check_atom_percolating(atoms, cut_off_neb, ind, tol=1)
        print("Ind:", ind, "percolating: ", percolating)
        if not percolating:
            print(f"Found a non-percolating path for {ion} in {formula}, consider not running this structure")
            continue

    user_args = {'material_id': material_id,
                 'magstate': magstate,
                 'ion': ion,
                 'cut_off_neb': cut_off_neb,
                 'thresholds': {
                     'ch_energy': ch_energy,
                     'potential': potential,
                     'diffusivity': diffusivity,
                     'volume_change': volume_change,
                     }
                 }
    with open('current_material.json', 'w') as f:
        json.dump(user_args, f, indent=4)
    #subprocess.run('mq workflow workflow_start.py', shell = True)
